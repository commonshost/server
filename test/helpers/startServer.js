const { spawn } = require('child_process')
const { createInterface } = require('readline')
const { once } = require('events')
const { join, resolve } = require('path')
const spy = require('through2-spy')
const mergeStream = require('merge-stream')

module.exports.startServer = async ({
  command = process.execPath,
  args = [],
  message = '',
  cwd = join(__dirname, '../fixtures'),
  throwOnCrash = true
} = {}) => {
  const bin = resolve(__dirname, '../../bin.js')
  const argv = [...process.execArgv, '--', bin]
  const child = spawn(command, [...argv, ...args], { cwd, throwOnCrash })
  const logger = spy((chunk) => {
    process.stdout.write(chunk)
  })

  function cleanup () { child.kill() }
  process.once('exit', cleanup)
  child.once('close', (code) => {
    process.removeListener('exit', cleanup)
    if (code !== 0 && code !== null && throwOnCrash === true) {
      throw new Error(`Server exited with error code ${code}`)
    }
  })

  const merged = mergeStream(child.stderr, child.stdout)
  const log = createInterface({ input: merged.pipe(logger) })
  for (
    let line = '';
    !line.includes(message);
    [line] = await once(log, 'line')
  ) continue

  return child
}
