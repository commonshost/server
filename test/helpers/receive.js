const { receiveHttp2: h2 } = require('./receive-http2')
const { receiveHttp1: h1 } = require('./receive-http1')

module.exports = { h1, h2 }
