const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h2 } = require('./helpers/receive')
const { createSocket } = require('dgram')
const packet = require('dns-packet')

const dnsQuery = (fields) => packet.encode({
  type: 'query',
  questions: [{
    type: 'A',
    name: 'example.com'
  }],
  ...fields
})

const dnsAnswer = (fields) => packet.encode({
  type: 'response',
  answers: [{
    type: 'A',
    class: 'IN',
    flush: true,
    name: 'example.com',
    data: '127.0.0.1'
  }],
  ...fields
})

let resolver
test('start mock dns resolver', async (t) => {
  resolver = createSocket('udp4', (message, { port }) => {
    const { id } = packet.decode(message)
    const response = dnsAnswer({ id })
    resolver.send(response, port)
  })
  resolver.bind()
})

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    doh: {
      resolverPort: resolver.address().port,
      resolverAddress: '127.0.0.1'
    },
    hosts: [{
      accessControl: {
        allowOrigin: '*'
      }
    }]
  }
  master = new Master({ cwd, options })
  await master.listen()
})

test('Make a DNS over HTTPS request, with CORS', async (t) => {
  const url = 'https://localhost:8443/'
  const headers = {
    ':method': 'POST',
    accept: 'application/dns-message'
  }
  const body = dnsQuery()
  const response = await h2(url, { headers, body })
  t.is(response.headers.get(':status'), 200)
  t.is(response.headers.get('access-control-allow-origin'), '*')
  t.deepEquals(
    packet.decode(await response.arrayBuffer()),
    packet.decode(dnsAnswer())
  )
})

test('Pass-through non-DoH requests', async (t) => {
  const url = 'https://localhost:8443/'
  const headers = { 'content-type': 'text/html; charset=utf-8' }
  const response = await h2(url, { headers })
  t.is(response.headers.get(':status'), 200)
  t.is(response.headers.get('content-type'), 'text/html; charset=utf-8')
  t.ok(await response.text())
})

test('Only respond to DoH on hosted domains', async (t) => {
  const url = 'https://does-not-exist.example.com:8443/'
  const headers = {
    ':method': 'POST',
    accept: 'application/dns-message'
  }
  const body = dnsQuery()
  const response = await h2(url, { headers, body })
  t.is(response.headers.get(':status'), 404)
  t.is(response.headers.get('content-type'), 'text/plain; charset=utf-8')
})

test('stop server', async (t) => master.close())
test('stop resolver', (t) => resolver.close(t.end))
