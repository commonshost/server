const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { hostname } = require('os')
const { h1 } = require('./helpers/receive')
const { createServer } = require('http')

const fixture = {
  body: Math.random().toString(10),
  via: `${hostname()} (Foo Bar)`
}

let acme
let master

test('start mock ACME server', (t) => {
  acme = createServer((request, response) => {
    t.is(request.url, '/.well-known/acme-challenge/foo')
    response.statusCode = 200
    if (request.headers['via']) {
      response.setHeader('via', request.headers['via'])
    }
    response.write(fixture.body)
    response.end()
  })
  acme.listen(t.end)
})

test('start server proxying ACME to mock server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    acme: { proxy: `http://localhost:${acme.address().port}` },
    via: { pseudonym: fixture.via }
  }
  master = new Master({ cwd, options })
  await master.listen()
})

test('Via header over HTTP/1', async (t) => {
  const url = 'http://localhost:8080/.well-known/acme-challenge/foo'
  const response = await h1(url)
  t.is(response.status, 200)
  const expected = `1.1 ${fixture.via}`
  t.is(response.headers.get('via'), expected)
  t.is(await response.text(), fixture.body)
})

test('Prevent ACME proxy loops', async (t) => {
  const url = 'http://localhost:8080/.well-known/acme-challenge/foo'
  const headers = { via: `1.1 ${fixture.via}` }
  const response = await h1(url, { headers })
  t.is(response.status, 508)
  t.is(await response.text(), 'Error')
})

test('stop server', async (t) => master.close())
test('stop acme', (t) => acme.close(t.end))

test('start server with misconfigured ACME proxy loop', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    acme: { proxy: `http://localhost:8080` },
    via: { pseudonym: fixture.via }
  }
  master = new Master({ cwd, options })
  await master.listen()
})

test('Prevent ACME proxy loops', async (t) => {
  const url = 'http://localhost:8080/.well-known/acme-challenge/foo'
  const headers = { via: `1.1 ${fixture.via}` }
  const response = await h1(url, { headers })
  t.is(response.status, 508)
  t.is(await response.text(), 'Error')
})

test('stop server', async (t) => master.close())
