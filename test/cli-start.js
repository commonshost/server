const test = require('blue-tape')
const { join } = require('path')
const { startServer } = require('./helpers/startServer')
const { h2: receive } = require('./helpers/receive')
const { once } = require('events')

function hasRequestPath (path) {
  return ({ request }) => {
    return request[':path'] === path
  }
}

test('Start with a valid external manifest', async (t) => {
  const args = ['start', '--options', 'external-manifest/config.json']
  const message = 'Server started'
  const node = await startServer({ args, message })
  const url = 'https://localhost:8443'
  const response = await receive(url)
  const OK = 200
  t.equal(response.headers.get(':status'), OK)
  t.ok(response.push.some(hasRequestPath('/200.html')))
  t.ok(response.push.some(hasRequestPath('/404.html')))
  node.kill()
  await once(node, 'exit')
})

test('Start with a non-existent external manifest', async (t) => {
  const args = ['start', '--options', 'external-manifest/missing-manifest.json']
  const message = 'Error: Cannot find module'
  const node = await startServer({ args, message, throwOnCrash: false })
  await once(node, 'exit')
})

test('Start with default configuration loads default manifest', async (t) => {
  const cwd = join(__dirname, 'fixtures/default-configuration')
  const args = ['start']
  const message = 'Server started'
  const node = await startServer({ args, message, cwd })
  const url = 'https://localhost:8443'
  const response = await receive(url)
  const OK = 200
  t.equal(response.headers.get(':status'), OK)
  t.equal(response.push.length, 1)
  t.ok(response.push.some(hasRequestPath('/app.js')))
  node.kill()
  await once(node, 'exit')
})
