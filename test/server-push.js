const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { connect } = require('http2')
const after = require('lodash.after')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    hosts: [{
      manifest: [{ glob: '**/*.html', push: '**/*.{js,txt}' }]
    }]
  }
  master = new Master({ cwd, options })
  await master.listen()
})

test('HTTP/2 GET with Server Push', (t) => {
  const expected = {
    pushRequests: new Set([
      {
        ':path': encodeURI('/foo.💩.js'),
        ':authority': 'localhost:8443',
        ':scheme': 'https',
        ':method': 'GET'
      },
      {
        ':path': '/stuff.txt',
        ':authority': 'localhost:8443',
        ':scheme': 'https',
        ':method': 'GET'
      }
    ]),
    pushResponses: new Set([
      {
        ':status': 200,
        'content-type': 'application/javascript',
        'cache-control': 'public, max-age=1, must-revalidate',
        'content-length': '70'
      },
      {
        ':status': 200,
        'content-type': 'text/plain; charset=utf-8',
        'cache-control': 'public, max-age=1, must-revalidate',
        'content-length': '10'
      }
    ]),
    response: {
      ':status': 200,
      'content-type': 'text/html; charset=utf-8',
      'cache-control': 'public, max-age=1, must-revalidate',
      'content-length': '111'
    }
  }

  const session = connect(
    'https://localhost:8443',
    { rejectUnauthorized: false }
  )

  const done = after(
    expected.pushResponses.size + 1,
    () => session.close()
  )

  const request = session.request({ ':path': '/' })

  session.on('stream', (stream, headers, flags) => {
    stream.on('push', (headers, flags) => {
      t.is(headers.server, undefined)
      const expectedPushResponse = Array.from(expected.pushResponses)
        .find((pushResponse) => {
          return Object.keys(pushResponse).every((header) => {
            return pushResponse[header] === headers[header]
          })
        })
      t.ok(expectedPushResponse)
      expected.pushResponses.delete(expectedPushResponse)
      stream.resume()
      stream.on('close', done)
    })
    for (const pushRequest of expected.pushRequests) {
      if (pushRequest[':path'] === headers[':path']) {
        t.deepEqual(headers, pushRequest)
        expected.pushRequests.delete(pushRequest)
        return
      }
    }
    t.fail('Mismatched PUSH_PROMISE :path pseudo-header')
  })

  request.on('response', (headers) => {
    for (const name of Object.keys(expected.response)) {
      t.is(headers[name], expected.response[name])
    }
    t.ok(headers.server.includes('node'))
    request.resume()
    request.on('end', request.close)
  })

  request.on('error', t.end)
  request.on('close', done)

  session.on('socketError', t.end)
  session.on('error', t.end)
  session.on('close', () => {
    t.is(expected.pushRequests.size, 0)
    t.is(expected.pushResponses.size, 0)
    t.end()
  })
})

test('stop server', async (t) => master.close())
