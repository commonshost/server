const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { connect } = require('http2')
const { h2: receive } = require('./helpers/receive')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    hosts: [{
      manifest: [{ get: '/index.html', push: '/stuff.txt' }]
    }]
  }
  master = new Master({ cwd, options })
  await master.listen()
})

const url = 'https://localhost:8443/'

let session
test('Establish HTTP/2 connection', async (t) => {
  session = connect(url, { rejectUnauthorized: false })
})

test('First request gets server push', async (t) => {
  const response = await receive(url, { session })
  t.equal(response.headers.get(':status'), 200)
  t.is(response.push.length, 1)
})

test('Second request skips server push', async (t) => {
  const response = await receive(url, { session })
  t.equal(response.headers.get(':status'), 200)
  t.is(response.push.length, 0)
})

test('Close HTTP/2 connection', (t) => {
  session.close(t.end)
})

test('stop server', async (t) => master.close())
