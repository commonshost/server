const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h2 } = require('./helpers/receive')

const fixtures = [
  {
    host: { fallback: { 200: '/200.html' } },
    requests: new Map([
      [
        'Serve fallback on missing extension',
        { path: '/foobar', status: 200, payload: 'Fallback found' }
      ],
      [
        'Serve fallback on HTML file',
        { path: '/foobar.html', status: 200, payload: 'Fallback found' }
      ],
      [
        'Serve fallback on directory',
        { path: '/foobar', status: 200, payload: 'Fallback found' }
      ],
      [
        'Serve 404 error on generic file extension',
        { path: '/foo.bar', status: 404, payload: 'Not Found' }
      ]
    ])
  },
  {
    host: {
      fallback: {
        200: './200.html',
        extensions: []
      }
    },
    requests: new Map([
      [
        'Serve fallback from relative file path',
        { path: '/foobar', status: 200, payload: 'Fallback found' }
      ],
      [
        'Serve fallback for Gopher URL (trailing slash)',
        {
          path: '/gopher://example.com/',
          status: 200,
          payload: 'Fallback found'
        }
      ],
      [
        'Serve fallback for Gopher URL (no trailing slash)',
        {
          path: '/gopher://example.com',
          status: 200,
          payload: 'Fallback found'
        }
      ]
    ])
  },
  {
    host: { fallback: { 404: '/404.html' } },
    requests: new Map([
      [
        'Serve 404 fallback on missing extension',
        { path: '/foobar', status: 404, payload: 'Fallback missing' }
      ],
      [
        'Serve 404 fallback on HTML file',
        { path: '/foobar.html', status: 404, payload: 'Fallback missing' }
      ],
      [
        'Serve 404 fallback on directory',
        { path: '/foobar', status: 404, payload: 'Fallback missing' }
      ],
      [
        'Serve 404 fallback on generic file extension',
        { path: '/foo.bar', status: 404, payload: 'Fallback missing' }
      ]
    ])
  },
  {
    host: { fallback: { 200: '/200.html', extensions: [] } },
    requests: new Map([
      [
        'Serve 200 fallback for any extension',
        { path: '/foo.bar', status: 200, payload: 'Fallback found' }
      ],
      [
        'Serve 200 fallback for no extension',
        { path: '/foobar', status: 200, payload: 'Fallback found' }
      ],
      [
        'Serve 200 fallback for trailing slash directory',
        { path: '/foobar/', status: 200, payload: 'Fallback found' }
      ]
    ])
  }
]

for (const { host, requests } of fixtures) {
  let master
  test('start server', async (t) => {
    const cwd = join(__dirname, 'fixtures')
    const options = { hosts: [ host ] }
    master = new Master({ cwd, options })
    await master.listen()
  })

  for (const [label, { path, status, payload }] of requests) {
    test(label, async (t) => {
      const url = `https://localhost:8443${path}`
      const response = await h2(url)
      t.notOk(response.headers.get('non-ascii'))
      t.is(response.headers.get(':status'), status)
      t.is(await response.text(), payload)
    })
  }

  test('stop server', async (t) => master.close())
}
