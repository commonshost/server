const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h1, h2 } = require('./helpers/receive')

let master

// Fallback to x080

test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {}
  process.env.PORT = 7777
  master = new Master({ cwd, options })
  await master.listen()
})

test('Listening for HTTPS on $PORT', async (t) => {
  const url = 'https://localhost:7777/'
  const response = await h2(url)
  t.is(response.headers.get(':status'), 200)
})

test('HTTP redirect on derived x080 port', async (t) => {
  const url = 'http://localhost:7080/'
  const response = await h1(url)
  t.is(response.status, 308)
  t.is(response.headers.get('location'), 'https://localhost:7777/')
})

test('stop server', async (t) => master.close())

// Fallback to x000
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {}
  process.env.PORT = 7080
  master = new Master({ cwd, options })
  await master.listen()
})

test('HTTP redirect on derived x080 fallback port', async (t) => {
  const url = 'http://localhost:7000/'
  const response = await h1(url)
  t.is(response.status, 308)
  t.is(response.headers.get('location'), 'https://localhost:7080/')
})

test('stop server', async (t) => master.close())

test('reset env $PORT', async (t) => { delete process.env.PORT })
