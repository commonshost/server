const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { connect } = require('tls')
const { once } = require('events')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    workers: { count: 10 }
  }
  master = new Master({ cwd, options })
  await master.listen()
})

for (const version of ['TLSv1.2', 'TLSv1.3']) {
  test(`Resume ${version} sessions across multiple workers`, async (t) => {
    let session
    const attempts = 10
    for (let i = 0; i < attempts; i++) {
      console.log(`Connecting ${i + 1}/${attempts} using ${version}`)
      const socket = connect({
        host: '127.0.0.1',
        port: 8443,
        rejectUnauthorized: false,
        servername: 'localhost',
        minVersion: version,
        maxVersion: version,
        session
      })
      const isFirstSession = i === 0
      await once(socket, 'secureConnect')
      t.is(socket.isSessionReused(), !isFirstSession)
      switch (socket.getProtocol()) {
        case 'TLSv1.2':
          session = socket.getSession()
          break
        case 'TLSv1.3':
          if (isFirstSession) {
            [session] = await once(socket, 'session')
          }
          break
        default:
          t.fail('Unsupported TLS version')
      }
      socket.end()
      await once(socket, 'close')
    }
  })
}

test('stop server', async (t) => master.close())
