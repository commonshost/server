const test = require('blue-tape')
const { startServer } = require('./helpers/startServer')
const { once } = require('events')

let server
test('Start CLI server', async (t) => {
  const args = ['start']
  const message = 'Server started'
  server = await startServer({ args, message })
})

test('Reload CLI server', async (t) => {
  const args = ['reload']
  const message = 'Reloading process'
  const node = await startServer({ args, message })
  await once(node, 'exit')
})

test('Stop CLI server', async (t) => {
  const args = ['stop']
  const message = 'Stopping process'
  const node = await startServer({ args, message })
  await Promise.all([
    once(node, 'exit'),
    once(server, 'exit')
  ])
})
