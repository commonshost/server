const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h1, h2 } = require('./helpers/receive')

const expected = new RegExp(
  '^' +
  'commonshost \\d+\\.\\d+\\.\\d+, ' +
  'nodejs \\d+\\.\\d+\\.\\d+, ' +
  '.+' + // OS name & version
  '$'
)

let master

test('start server with Via header pseudonym', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = { signature: true }
  master = new Master({ cwd, options })
  await master.listen()
})

test('Server header over plaintext HTTP/1', async (t) => {
  const url = 'http://localhost:8080/'
  const response = await h1(url)
  const actual = response.headers.get('server')
  t.ok(expected.test(actual))
})

test('Server header over HTTP/2', async (t) => {
  const url = 'https://localhost:8443/'
  const response = await h2(url)
  const actual = response.headers.get('server')
  t.ok(expected.test(actual))
})

test('stop server', async (t) => master.close())
