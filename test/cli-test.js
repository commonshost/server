const test = require('blue-tape')
const { startServer } = require('./helpers/startServer')
const { once } = require('events')

test('Test a valid configuration', async (t) => {
  const args = ['test']
  const message = 'PASS'
  const node = await startServer({ args, message })
  await once(node, 'exit')
})

test('Test an invalid configuration', async (t) => {
  const args = ['test', '--options', 'does-not-exist']
  const message = 'FAIL: Cannot find module'
  const node = await startServer({ args, message, throwOnCrash: false })
  await once(node, 'exit')
})

test('Test a valid external manifest', async (t) => {
  const args = ['test', '--options', 'external-manifest/config.json']
  const message = 'PASS'
  const node = await startServer({ args, message })
  await once(node, 'exit')
})

test('Test a non-existent external manifest', async (t) => {
  const args = ['test', '--options', 'external-manifest/missing-manifest.json']
  const message = 'FAIL: Cannot find module'
  const node = await startServer({ args, message, throwOnCrash: false })
  await once(node, 'exit')
})
