const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h2 } = require('./helpers/receive')
const { createServer } = require('net')
const { once } = require('events')

let gopherServer
test('start mock Gopher server', async (t) => {
  gopherServer = createServer((socket) => {
    socket.once('data', (chunk) => {
      socket.end('1Hello, World!\t\t\t\r\n.\r\n')
    })
  })
  gopherServer.listen(0)
  await once(gopherServer, 'listening')
})

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    goh: {
      allowHTTP1: false,
      unsafeAllowNonStandardPort: true,
      unsafeAllowPrivateAddress: true,
      timeout: 10000
    },
    hosts: [{
      accessControl: {
        allowOrigin: '*'
      }
    }]
  }
  master = new Master({ cwd, options })
  await master.listen()
})

test('Make a Gopher over HTTP request', async (t) => {
  const gopherUrl = `gopher://localhost:${gopherServer.address().port}/1/`
  const url = `https://localhost:8443/?url=${encodeURIComponent(gopherUrl)}`
  const headers = { accept: 'application/gopher' }
  const response = await h2(url, { headers })
  t.ok(response.ok)
  t.is(response.status, 200)
  t.is(response.headers.get('content-type'), 'application/gopher')
  t.is(response.headers.get('access-control-allow-origin'), '*')
  t.deepEquals(await response.text(), '1Hello, World!\t\t\t\r\n.\r\n')
})

test('stop server', async (t) => master.close())
test('stop mock Gopher server', (t) => gopherServer.close(t.end))
