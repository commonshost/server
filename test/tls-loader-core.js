const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { connect } = require('tls')
const { createServer } = require('http')
const { once } = require('events')
const { promises: { readFile } } = require('fs')
const userHome = require('user-home')

let core
test('start mock Core API', async (t) => {
  core = createServer(async (request, response) => {
    console.log('[core]', request.method, request.url)
    t.is(request.method, 'GET')
    t.is(request.url, '/v2/sites/localhost/certificate')
    t.is(request.headers['authorization'], 'Bearer access-token')
    const cert = join(userHome, '.commonshost/cert.pem')
    const key = join(userHome, '.commonshost/key.pem')
    const body = {
      servername: 'localhost',
      ca: [],
      cert: await readFile(cert, 'utf8'),
      key: await readFile(key, 'utf8')
    }
    response.end(JSON.stringify(body))
  })
  core.listen()
  await once(core, 'listening')
})

let issuer
test('start mock JWT issuer', async (t) => {
  issuer = createServer(async (request, response) => {
    console.log('[issuer]', request.method, request.url)
    t.is(request.method, 'POST')
    t.is(request.url, '/oauth/token')
    const body = []
    for await (const chunk of request) body.push(chunk)
    t.deepEqual(JSON.parse(Buffer.concat(body)), {
      grant_type: 'client_credentials',
      audience: 'https://example.net/',
      scope: 'global_read',
      client_id: 'client-id',
      client_secret: 'client-secret'
    })
    response.end(JSON.stringify({ access_token: 'access-token' }))
  })
  issuer.listen()
  await once(issuer, 'listening')
})

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    tls: { loader: 'core' },
    gopher: {
      port: 7000,
      plaintextFallback: join(cwd, 'gopher/fallback.txt'),
      secureRoot: join(cwd, 'gopher/sites/$domain/public/_gopher')
    },
    core: {
      origin: `http://localhost:${core.address().port}`,
      auth0: {
        issuer: `http://localhost:${issuer.address().port}/`,
        audience: 'https://example.net/',
        clientId: 'client-id',
        clientSecret: 'client-secret'
      }
    }
  }
  master = new Master({ cwd, options })
  await master.listen()
})

test(`Use SNI to connect HTPT/2 over TLS`, async (t) => {
  const client = connect({
    port: 8443,
    servername: 'localhost',
    ALPNProtocols: ['h2'],
    rejectUnauthorized: false
  })
  await once(client, 'secureConnect')
  t.is(client.alpnProtocol, 'h2')
  client.end()
  await once(client, 'close')
})

test(`Use SNI to connect Gopher over TLS`, async (t) => {
  const client = connect({
    port: 7000,
    servername: 'localhost',
    ALPNProtocols: ['gopher'],
    rejectUnauthorized: false
  })
  await once(client, 'secureConnect')
  t.is(client.alpnProtocol, 'gopher')
  client.end()
  await once(client, 'close')
})

test('stop mock JWT issuer', async (t) => issuer.close())
test('stop mock Core API', async (t) => core.close())
test('stop server', async (t) => master.close())
