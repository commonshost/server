const { join } = require('path')

module.exports = {
  workers: {
    count: 'max_physical_cpu_cores / 2'
  },
  acme: {
    redirect: 'http://localhost:12345/',
    webroot: join(__dirname, 'acme/webroot')
  },
  tls: {
    loader: 'file',
    storePath: join(__dirname, 'acme/store'),
    keyPath: '$store/$domain/key.pem',
    certPath: '$store/$domain/key.pem'
  },
  hosts: [
    {
      domain: 'localhost',
      root: join(__dirname, 'public'),
      fallback: {
        200: '/200.html',
        404: '/404.html'
      },
      manifest: [
        {
          glob: '**/*.html',
          push: [
            {
              glob: [
                '**/*',
                '!**/*.{map,html}',
                '!**/service-worker/**/*'
              ],
              priority: 256
            }
          ]
        }
      ]
    }
  ]
}
