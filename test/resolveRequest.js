const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h1 } = require('./helpers/receive')

const fixtures = [
  {
    resolve: true,
    given: 'https://localhost:8443/foo/../../bar//logo.svg',
    expected: {
      status: 308,
      statusText: 'Permanent Redirect',
      location: 'https://localhost:8443/bar/logo.svg'
    }
  },
  {
    resolve: false,
    given: 'https://localhost:8443/foo/../../bar//logo.svg',
    expected: {
      status: 404,
      statusText: 'Not Found',
      location: null
    }
  },
  {
    resolve: undefined,
    given: 'https://localhost:8443/foo/../../bar//logo.svg',
    expected: {
      status: 308,
      statusText: 'Permanent Redirect',
      location: 'https://localhost:8443/bar/logo.svg'
    }
  },
  {
    resolve: false,
    given: 'https://localhost:8443/gopher://1/foo/bar%09lol%20abc',
    expected: {
      status: 404,
      statusText: 'Not Found',
      location: null
    }
  }
]

for (const { resolve, given, expected } of fixtures) {
  let master
  test('start server', async (t) => {
    const cwd = join(__dirname, 'fixtures')
    const options = { hosts: [{ directories: { resolve } }] }
    master = new Master({ cwd, options })
    await master.listen()
  })

  test('Resolve special URL paths safely', async (t) => {
    const url = given
    const response = await h1(url)
    t.is(response.status, expected.status)
    t.is(response.statusText, expected.statusText)
    t.is(
      response.headers.get('location'),
      expected.location
    )
  })

  test('stop server', async (t) => master.close())
}
