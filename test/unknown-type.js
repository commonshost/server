const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h1 } = require('./helpers/receive')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures/unknown-file-type')
  const options = {}
  master = new Master({ cwd, options })
  await master.listen()
})

test('Unknown file type', async (t) => {
  const url = 'https://localhost:8443/file.unknown'
  const response = await h1(url)
  t.is(response.status, 200)
  t.is(response.headers['content-type'], undefined)
  t.is(await response.text(), 'Stuff')
})

test('stop server', async (t) => master.close())
