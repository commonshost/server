const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { connect } = require('http2')

const expected = 1000
const drift = 0.1 // Acceptable margin because timers are imprecise

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    http2: {
      timeout: expected
    }
  }
  master = new Master({ cwd, options })
  await master.listen()
})

test('Set custom timeout for HTTP/2 connections', (t) => {
  t.plan(1)
  const url = 'https://localhost:8443'
  const options = { rejectUnauthorized: false }
  connect(url, options, (session) => {
    const headers = {}
    const stream = session.request(headers)
    stream.resume()
    stream.on('close', () => {
      const start = Date.now()
      const cutoff = setTimeout(() => {
        t.fail('Too slow')
        session.destroy()
      }, expected * (1 + drift))
      session.on('close', () => {
        clearTimeout(cutoff)
        const actual = Date.now() - start
        t.ok(actual > expected * (1 - drift))
      })
    })
  })
})

test('stop server', async (t) => master.close())
