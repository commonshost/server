const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { connect } = require('http2')
const { h1 } = require('./helpers/receive')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {}
  master = new Master({ cwd, options })
  await master.listen()
})

test('HTTP/2 unknown domain', (t) => {
  const session = connect(
    'https://foobar:8443',
    {
      rejectUnauthorized: false,
      lookup: (hostname, options, callback) => {
        callback(null, '127.0.0.1', 4)
      }
    }
  )

  const stream = session.request({ ':path': '/' })

  stream.on('response', (headers) => {
    t.is(headers[':status'], 404)
    stream.resume()
    stream.on('end', () => stream.close())
    stream.on('close', () => session.close(t.end))
  })

  session.on('socketError', t.end)
  session.on('error', t.end)
  stream.on('error', t.end)
})

test('HTTP/1.1 unknown domain', async (t) => {
  const url = 'https://foobar:8443/'
  const response = await h1(url)
  t.is(response.status, 404)
  t.is(response.statusText, 'Not Found')
})

test('stop server', async (t) => master.close())
