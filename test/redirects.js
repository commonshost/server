const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h2 } = require('./helpers/receive')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    hosts: [{
      redirects: [
        { from: '/relative', to: '/expected' },
        { from: '/relative-dot', to: './expected' },
        { from: '/remote-absolute', to: 'https://example.net/expected' },
        { from: '/default-status', to: '/expected' },
        { from: '/custom-status', to: '/expected', status: 302 },
        { from: '/substitution{/pattern}', to: '/expected{/pattern}' }
      ]
    }]
  }
  master = new Master({ cwd, options })
  await master.listen()
})

test('redirect relative', async (t) => {
  const url = 'https://localhost:8443/relative'
  const response = await h2(url)
  const actual = response.headers.get('location')
  const expected = 'https://localhost:8443/expected'
  t.is(actual, expected)
})

test('redirect relative', async (t) => {
  const url = 'https://localhost:8443/relative-dot'
  const response = await h2(url)
  const actual = response.headers.get('location')
  const expected = 'https://localhost:8443/expected'
  t.is(actual, expected)
})

test('redirect remote absolute', async (t) => {
  const url = 'https://localhost:8443/remote-absolute'
  const response = await h2(url)
  const actual = response.headers.get('location')
  const expected = 'https://example.net/expected'
  t.is(actual, expected)
})

test('redirect default status', async (t) => {
  const url = 'https://localhost:8443/default-status'
  const response = await h2(url)
  t.is(response.status, 308)
})

test('redirect custom status', async (t) => {
  const url = 'https://localhost:8443/custom-status'
  const response = await h2(url)
  t.is(response.status, 302)
})

test('redirect substitution pattern', async (t) => {
  const url = 'https://localhost:8443/substitution/given'
  const response = await h2(url)
  const actual = response.headers.get('location')
  const expected = 'https://localhost:8443/expected/given'
  t.is(actual, expected)
})

test('stop server', async (t) => master.close())
