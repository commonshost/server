const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h1 } = require('./helpers/receive')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {}
  master = new Master({ cwd, options })
  await master.listen()
})

test('Serve full path for index.html', async (t) => {
  const url = 'https://localhost:8443/index.html'
  const response = await h1(url)
  t.is(response.status, 200)
  t.is(response.statusText, 'OK')
  t.is(response.headers.get('content-length'), '94')
})

test('Serve shorthand path for index.html', async (t) => {
  const url = 'https://localhost:8443/'
  const response = await h1(url)
  t.is(response.status, 200)
  t.is(response.statusText, 'OK')
  t.is(response.headers.get('content-length'), '94')
})

test('stop server', async (t) => master.close())
