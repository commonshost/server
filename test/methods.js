const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h1, h2 } = require('./helpers/receive')
const { constants: {
  HTTP_STATUS_OK,
  HTTP_STATUS_BAD_REQUEST,
  HTTP_STATUS_PERMANENT_REDIRECT,
  HTTP_STATUS_METHOD_NOT_ALLOWED
} } = require('http2')

const http = 'http://localhost:8080/'
const https = 'https://localhost:8443/'

const fixtures = [
  { agent: h1, url: http, method: 'GET', status: HTTP_STATUS_PERMANENT_REDIRECT },
  { agent: h1, url: http, method: 'HEAD', status: HTTP_STATUS_PERMANENT_REDIRECT },
  { agent: h1, url: http, method: 'OPTIONS', status: HTTP_STATUS_PERMANENT_REDIRECT },
  { agent: h1, url: http, method: 'POST', status: HTTP_STATUS_METHOD_NOT_ALLOWED },
  { agent: h1, url: http, method: 'PUT', status: HTTP_STATUS_METHOD_NOT_ALLOWED },
  { agent: h1, url: http, method: 'PATCH', status: HTTP_STATUS_METHOD_NOT_ALLOWED },

  { agent: h1, url: https, method: 'GET', status: HTTP_STATUS_OK },
  { agent: h1, url: https, method: 'HEAD', status: HTTP_STATUS_OK },
  { agent: h1, url: https, method: 'OPTIONS', status: HTTP_STATUS_OK },
  { agent: h1, url: https, method: 'POST', status: HTTP_STATUS_METHOD_NOT_ALLOWED },
  { agent: h1, url: https, method: 'PUT', status: HTTP_STATUS_METHOD_NOT_ALLOWED },
  { agent: h1, url: https, method: 'PATCH', status: HTTP_STATUS_METHOD_NOT_ALLOWED },

  { agent: h2, url: https, method: 'GET', status: HTTP_STATUS_OK },
  { agent: h2, url: https, method: 'HEAD', status: HTTP_STATUS_OK },
  { agent: h2, url: https, method: 'OPTIONS', status: HTTP_STATUS_OK },
  { agent: h2, url: https, method: 'POST', status: HTTP_STATUS_METHOD_NOT_ALLOWED },
  { agent: h2, url: https, method: 'PUT', status: HTTP_STATUS_METHOD_NOT_ALLOWED },
  { agent: h2, url: https, method: 'PATCH', status: HTTP_STATUS_METHOD_NOT_ALLOWED }
]

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {}
  master = new Master({ cwd, options })
  await master.listen()
})

for (const { agent, method, url, status } of fixtures) {
  const protocols = new Map([[h1, 'HTTP/1'], [h2, 'HTTP/2']])
  const protocol = protocols.get(agent)
  test(`${protocol} ${method} ${url}`, async (t) => {
    const response = await agent(url, { method })
    t.is(response.status, status)
  })
}

test('Reject unknown HTTP methods', async (t) => {
  let response
  try {
    response = await h1(http, { method: 'FOOBAR' })
    t.is(response.status, HTTP_STATUS_BAD_REQUEST) // Node.js 10+
  } catch ({ code }) {
    t.is(code, 'ECONNRESET') // Node.js 8
  }
})

test('stop server', async (t) => master.close())
