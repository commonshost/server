const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h2 } = require('./helpers/receive')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    hosts: [{
      domain: 'example.net',
      headers: [
        {
          uri: '/foo.{emoji}.js',
          fields: {
            'non-ascii': '{emoji}',
            'ascii-only': 'foo'
          }
        },
        {
          fields: {
            'always-set-without-predicate': 'bar'
          }
        },
        {
          uri: '/stuff.txt',
          fields: {
            'Mixed-Case': 'foo',
            'mixed-case': 'bar',
            'MIXED-CASE': 'bla'
          }
        },
        {
          uri: '/stuff.txt{?query,}',
          fields: {
            'parse-query-string': 'query parameter is {query}'
          }
        }
      ]
    }]
  }
  master = new Master({ cwd, options })
  await master.listen()
})

test('Custom header must be ASCII only, ignored otherwise', async (t) => {
  const url = `https://example.net:8443/foo.${encodeURI('💩')}.js`
  const response = await h2(url)
  t.notOk(response.headers.get('non-ascii'))
  t.is(response.headers.get('ascii-only'), 'foo')
  t.is(response.headers.get('always-set-without-predicate'), 'bar')
})

test('Mixed-case equivalent headers are concatenated', async (t) => {
  const url = 'https://example.net:8443/stuff.txt'
  const response = await h2(url)
  t.ok(response.headers.get('mixed-case').includes('foo'))
  t.ok(response.headers.get('mixed-case').includes('bar'))
  t.ok(response.headers.get('mixed-case').includes('bla'))
  t.notOk(response.headers.get('Mixed-Case'))
  t.notOk(response.headers.get('MIXED-CASE'))
})

test('Parse the query string and use values in field templates', async (t) => {
  const url = 'https://example.net:8443/stuff.txt?foo=bar&query=yep&then=end'
  const response = await h2(url)
  t.is(
    response.headers.get('parse-query-string'),
    'query parameter is yep'
  )
})

test('stop server', async (t) => master.close())
