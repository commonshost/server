const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { once } = require('events')
const tls = require('tls')
const net = require('net')

async function readAll (socket) {
  const chunks = []
  for await (const chunk of socket) {
    chunks.push(chunk)
  }
  return Buffer.concat(chunks)
}

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures/gopher')
  const options = {
    gopher: {
      port: 7000,
      plaintextFallback: join(cwd, 'fallback.txt'),
      secureRoot: join(cwd, '/sites/$domain/public/_gopher')
    }
  }
  master = new Master({ cwd, options })
  await master.listen()
})

const fixtures = [
  {
    selector: '',
    expected: 'Index of root dir'
  },
  {
    selector: '/',
    expected: 'Index of root dir'
  },
  {
    selector: '/gophermap',
    expected: 'Index of root dir'
  },
  {
    selector: '/bar.txt',
    expected: 'File in root dir'
  },
  {
    selector: '/does-not-exist',
    expected: '3File not found\t\terror.host\t1\r\n.\r\n'
  },
  {
    selector: '/subdir1',
    expected: 'Index of subdir1'
  },
  {
    selector: '/subdir1/',
    expected: 'Index of subdir1'
  },
  {
    selector: '/subdir1/gophermap',
    expected: 'Index of subdir1'
  },
  {
    selector: '/subdir2/foo.txt',
    expected: 'File in subdir2'
  },
  {
    selector: '/subdir2',
    expected: '3File not found\t\terror.host\t1\r\n.\r\n'
  },
  {
    selector: '/subdir3',
    expected: '3File not found\t\terror.host\t1\r\n.\r\n'
  },
  {
    selector: '/../../../../../../../package.json',
    expected: '3File not found\t\terror.host\t1\r\n.\r\n'
  }
]

for (const { selector, expected } of fixtures) {
  test(`Make a Gopher request: ${selector}`, async (t) => {
    const client = tls.connect({
      servername: 'example.com',
      port: 7000,
      ALPNProtocols: ['gopher'],
      rejectUnauthorized: false
    })
    await once(client, 'secureConnect')
    client.write(`${selector}\r\n`)
    const response = await readAll(client)
    t.is(response.toString(), expected)
  })
}

test('Reject non-SNI clients', async (t) => {
  const client = tls.connect({
    // servername: undefined,
    port: 7000,
    ALPNProtocols: ['gopher'],
    rejectUnauthorized: false
  })
  const [error] = await once(client, 'error')
  t.is(error.code, 'ECONNRESET')
})

test('Sanitise and accept malicious SNI', async (t) => {
  const client = tls.connect({
    servername: '../../../../../../../../../../../../../etc/passwd',
    port: 7000,
    ALPNProtocols: ['gopher'],
    rejectUnauthorized: false
  })
  await once(client, 'secureConnect')
  client.end()
  await once(client, 'close')
})

for (const selector of ['', '/whatever']) {
  test('Reject plaintext Gopher clients', async (t) => {
    const client = net.connect({ port: 7000 })
    await once(client, 'connect')
    client.write(`/${selector}\r\n`)
    const response = await readAll(client)
    t.is(response.toString(), 'Plaintext fallback content for any selector.')
  })
}

test('GoT client disconnects before response', async (t) => {
  const client = tls.connect({
    servername: 'example.com',
    port: 7000,
    ALPNProtocols: ['gopher'],
    rejectUnauthorized: false
  })
  await once(client, 'secureConnect')
  client.end('/\r\n')
  await once(client, 'close')
})

test('stop server', async (t) => master.close())
