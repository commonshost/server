const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const tmp = require('tmp-promise')
const { keygen } = require('tls-keygen')
const { connect } = require('tls')
const { readFileSync } = require('fs')

let scratch
test('Prepare certificate', async (t) => {
  scratch = await tmp.dir({ unsafeCleanup: true })
  await keygen({
    commonName: 'example.net',
    subjectAltName: ['DNS:example.net'],
    entrust: false,
    key: join(scratch.path, 'acme/example.net/key.pem'),
    cert: join(scratch.path, 'acme/example.net/cert.pem')
  })
})

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    workers: { count: 1 },
    tls: {
      storePath: join(scratch.path, 'acme'),
      keyPath: '$store/$domain/key.pem',
      certPath: '$store/$domain/cert.pem'
    }
  }
  master = new Master({ cwd, options })
  await master.listen()
})

test('Verify original certificate', (t) => {
  const socket = connect(8443, 'localhost', {
    ca: [readFileSync(join(scratch.path, 'acme/example.net/cert.pem'))],
    // rejectUnauthorized: false,
    servername: 'example.net',
    lookup: (hostname, options, callback) => {
      callback(null, '127.0.0.1', 4)
    },
    checkServerIdentity: (servername, cert) => {
      t.is(cert.subject.CN, 'example.net')
      t.is(cert.subjectaltname, 'DNS:example.net')
      socket.end(t.end)
    }
  })
})

test('Issue certificate with additional SAN', async (t) => {
  await keygen({
    commonName: 'example.net',
    subjectAltName: ['DNS:example.net', 'DNS:example.com'],
    entrust: false,
    key: join(scratch.path, 'acme/example.net/key.pem'),
    cert: join(scratch.path, 'acme/example.net/cert.pem')
  })
  await master.message({
    type: 'certificate-issue',
    domain: 'example.net',
    root: scratch.path
  })
})

test('Wait a bit', (t) => setTimeout(t.end, 1000))

test('Verify updated certificate', (t) => {
  const socket = connect(8443, 'localhost', {
    ca: [readFileSync(join(scratch.path, 'acme/example.net/cert.pem'))],
    // rejectUnauthorized: false,
    servername: 'example.net',
    lookup: (hostname, options, callback) => {
      callback(null, '127.0.0.1', 4)
    },
    checkServerIdentity: (servername, cert) => {
      t.is(cert.subject.CN, 'example.net')
      t.is(cert.subjectaltname, 'DNS:example.net, DNS:example.com')
      socket.end(t.end)
    }
  })
})

test('stop server', async (t) => master.close())
