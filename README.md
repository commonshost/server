# @commonshost/server

Static HTTP/2 web server for single page apps and progressive web apps.

## Key Features

- HTTP/2 with fallback to HTTP/1.x
- HTTP/2 Server Push Manifests
- Brotli and Gzip compression negotiation
- Multi-core clustering
- Multiple hosts with different domains
- Fallback for client side routing
- Custom response headers
- `http:` to `https:` URL redirect
- Auto-generate TLS certificate for localhost development
