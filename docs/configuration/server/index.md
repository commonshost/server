# Server Options

## `signature`

Default: `true`

Boolean value that adds the `Server:` header to all HTTP responses if `true`, and omits that header if `false`.

The value of the header identifies the versions of the server, Node.js, and the operating system.

Often disabled for reasons of paranoia or placebo performance tuning.

## `acme`

Settings related to the [Automated Certificate Management Environment (ACME)](https://datatracker.ietf.org/wg/acme/) protocol, i.e. support for the [LetsEncrypt](https://letsencrypt.org) certificate authority. This affects all requests, across all hosted domains, where the pathname starts with [`/.well-known/acme-challenge/`](https://www.iana.org/assignments/well-known-uris/well-known-uris.xhtml). There are many ways to implement ACME support. These settings offer flexibility in issuing certificates and serving the correct certificate for each host.

When a certificate is requested for a domain using the ACME HTTP challenge, the certificate authority (i.e. LetsEncrypt's [Boulder](https://github.com/letsencrypt/boulder) server) resolves the given DNS hostname and makes an HTTP request. The webserver needs to respond with values only known by the entity that requested the certificate. In a simple case of one webserver this can be handled by serving a local directory through `acme.webroot`. But when DNS records point to multiple edge servers, it is necessary to relay or deflect the challenge request, via `acme.proxy` or `acme.redirect` respectively, to the certificate requesting entity.

### `acme.proxy`

Default: `''`

If a valid URI is specified, the server proxies all ACME challenge requests to this upstream server.

Proxying requires the server to do more work than a redirect, but it is transparent to the CA. This allows use of ports other than `80` and `443`, to which Boulder is restricted.

This string is prepended to the challenge request's URL path, so omit any trailing shash to avoid duplication. Both HTTP and HTTPS can be used, depending on the scheme (`http:` or `https:`).

Example:

```js
{
  acme: {
    proxy: 'https://vault.example.net:8443'
  }
}
```

### `acme.redirect`

Default: `''`

If a valid URI is specified, the server redirects, through a [`301 Permanent Redirect`](https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml) response, all ACME challenge requests to this upstream server.

Redirecting is light on the edge server, but requires a second connection from Boulder to the specified server, possibly adding some latency.

Both HTTP and HTTPS can be used, depending on the scheme, i.e. `http:` or `https:` respectively. Note that Boulder, the LetsEncrypt reference server, is currently restricted to ports `80` and `443`. Redirecting to any other ports is not supported, and should be handled through `acme.proxy`.

This string is prepended to the challenge request's URL path. Omit any trailing slash to avoid duplication.

Example:

```js
{
  acme: {
    redirect: 'https://vault.example.net'
  }
}
```

### `acme.webroot`

Default: `''`

If a valid path is specified, the server responds to challenges by mapping to static files in this directory. This is intended to work with the `webroot` option of many ACME clients, where files are generated during the challenge-response process.

```js
{
  acme: {
    webroot: '/var/www/html'
  }
}
```

## `core`

Communicate with a backend API as a coordination service for multiple server instances. Used to retrieve hosted site configurations (TLS certificates, etc).

See: [@commonshost/core](https://gitlab.com/commonshost/core)

Example:

```js
{
  core: {
    origin: 'https://example.net:4433',
    auth0: {
      issuer: 'https://example.auth0.com/',
      audience: 'https://example.net/',
      clientId: 'QYvq8geQNBJlAf3FDTyd',
      clientSecret: 'FOzMGZYZxm...pkgf6RLQmx'
    }
  }
}
```

### `core.origin`

Default: `''`

The [URL protocol scheme, hostname and port](https://developer.mozilla.org/en-US/docs/Web/API/HTMLHyperlinkElementUtils/origin) of the Core API.

### `core.auth0`

Settings to use Auth0 as the authentication provider for communication with the Core API.

See: [Auth0 - How to implement the Client Credentials Grant](https://auth0.com/docs/api-auth/tutorials/client-credentials)

#### `core.auth0.accessToken`

Default: `''`

Example: `'eyJhbGciOiJIUzI...'`

The [JSON Web Token (JWT)](https://jwt.io) to use for authentication with the Core API. If left blank, will be generated using the `audience`, `clientId`, `clientSecret`, `issuer`, and `scopes` settings.

#### `core.auth0.audience`

Default: `''`

Provided by Auth0. The **Identifier** value on the [Settings](https://manage.auth0.com/#/apis) tab for the API.

#### `core.auth0.clientId`

Default: `''`

Provided by Auth0. See: https://tools.ietf.org/html/rfc6749#section-2.3.1

#### `core.auth0.clientSecret`

Default: `''`

Provided by Auth0. See: https://tools.ietf.org/html/rfc6749#section-2.3.1

#### `core.auth0.issuer`

Default: `''`

Provided by Auth0. The Auth0 domain with a `https://` prefix and a `/` suffix: `https://YOUR_DOMAIN/`.

#### `core.auth0.scope`

Default: `''`

See the Core API documentation for available scopes. Typically set to `'global_read'` for read access to multiple sites.

## `doh`

Default: `false`

DNS over HTTPS (DoH) support. Set to `true` for defaults (local DNS resolver) or an object containing custom configuration.

### `doh.protocol`

Default: `'udp4'`

Can be either `'udp4'` or `'udp6'` to indicate whether to connect to the resolver over IPv4 or IPv6 respectively.

### `doh.localAddress`

Default: `'0.0.0.0'` (IPv4) or `'::0'` (IPv6)

The UDP socket is bound to this address.

Use a loopback IP address (`''` empty string, `'localhost'`, `'127.0.0.1'`, or `'::1'`) to only accept local DNS resolver responses.

Use a wildcard IP address (`'0.0.0.0'` or `'::0'`) to accept remote DNS resolver responses.

### `doh.resolverAddress`

Default: `'127.0.0.1'` (IPv4) or `'::1'` (IPv6)

The IP address of the DNS resolver. Queries are sent via UDP.

See also: [List of public DNS service operators](https://en.wikipedia.org/wiki/Public_recursive_name_server) on Wikipedia.

### `doh.resolverPort`

Default: `53`

The port of the DNS resolver.

### `doh.timeout`

Default: `10000`

Number of milliseconds to wait for a response from the DNS resolver.

## `goh`

Default: `false`

[Gopher over HTTP](https://gopher.commons.host) (GoH) support. Set to `true` or an object to enable.

### `goh.allowHTTP1`


Default: `false`

If `false` only HTTP/2 (or later) clients are accepted. Set to `true` to also accept requests from HTTP/1 clients.

### `goh.unsafeAllowNonStandardPort`

Default: `false`

If `false` the relay is restricted to the standard Gopher port `70`. If `true` the middleware accepts URLs with any port number. Allowing any port is potentially unsafe and not recommended. The middleware does not validate the response, effectively becoming an open TCP/IP proxy.

### `goh.unsafeAllowPrivateAddress`

Default: `false`

If `false` connection attempts to any private IPv4 or IPv6 address are denied. This is important for security when operating a public GoH service to avoid exposing LAN hosts to malicious external users. Set to `true` to allow connections to remote hosts with private IP addresses.

### `goh.timeout`

Default: `10000`

The number of milliseconds to keep idle Gopher sessions active. Defaults to 10 seconds. The HTTP connection (aka session) is not closed, only the TCP/IP socket to the Gopher server and its corresponding HTTP/2 streams with the HTTP user agent.

## `gopher`

Default: `false`

If set to an object, enables serving of static files via the Gopher over TLS (GoT) protocol. No processing of the files is done. It is up to the publisher to ensure the file are Gopher-compliant. Notably, text documents and menus must be "terminated by a period on a line by itself." And specifically for text files: "Lines beginning with periods must be prepended with an extra period to ensure that the transmission is not terminated early."

### `gopher.port`

Default: `70`

Port to listen for Gopher connections. Negotiates between clients using either plaintext Gopher protocol or Gopher over TLS.

### `gopher.plaintextFallback`

Default: `''`

Path of a file to serve to non-TLS enabled Gopher clients. If this file is not found an error message is returned.

    3Secure connection required

Without Gopher over TLS support the domain a client is accessing is not know to the server. So only one plaintext Gopher site can be served per IP address. This plaintext fallback may be encourage users of legacy Gopher clients to upgrade to a client with Gopher over TLS support.

### `gopher.secureRoot`

Default: `'sites/$domain/public/_gopher'`

Expression defining the location of files to serve via Gopher over TLS. The variable `$domain` is substituted with the TLS SNI server name. The Gopher selector (aka URL) is resolved within this path. If the URL resolves to a directory, the `gophermap` index file is served, if it exists. If no file matches the selector an error message is returned.

## `log`

Settings for the [Pino](https://getpino.io)-based logger.

### `log.level`

Default: `'info'`

Minimum threshold for log messages to be recorded. One of `fatal`, `error`, `warn`, `info`, `debug`, `trace`; or `silent` to disable logging.

## `http`

An object containing settings for the automatic redirection of HTTP to HTTPS. This is necessary since HTTP/2 is only supported by browsers when using a TLS connection (TLS).

### `http.redirect`

Default: `true`

If `true`, an HTTP server listens to redirect requests to HTTPS.

If `false`, no HTTP server is started. This also means ACME is disabled, since it is served over HTTP as well.

### `http.from`

Default: `8080`

The port number where the HTTP server accepts connections.

### `http.to`

Default: `8443`

The port number of the HTTPS URLs to which HTTP traffic is redirected.

Redirects use the `308 Permanent Redirect` status code.

## `http2`

Tune HTTP/2 specific behaviour.

### `http2.timeout`

Default: `null` which falls back to the Node.js `http2` module default of 2 minutes.

Duration, in milliseconds as a number, after which idle HTTP/2 sessions are closed.

## `https`

Settings for HTTP over TLS.

### `https.port`

Default: `8443`

The TCP/IP port number for incoming TLS connections.

The [standard HTTPS port](https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml?search=https) is `443` for both HTTP/1.1 and HTTP/2. However, on Unix (Linux/MacOS), root permissions are required to listen on port numbers from 1 to 1024. The default value is outside this restricted range.

## `placeholder`

By default, requests for sites that are not configured under hosts are responded to with a `404` status and `Not Found` error message.

### `placeholder.hostNotFound`

File path to an HTML response to be served when a request is made to an unrecognised hostname. The response status code is `404`.

Only a single file is allowed. Any required assets should either be inlined or hosted on an external domain.

The placeholder is only served if the request accepts an HTML response and the pathname either has no file extension or ends in `.html`.

## `push`

The experimental push diary feature is a [Cuckoo filter](http://www.cs.cmu.edu/~binfan/papers/conext14_cuckoofilter.pdf) which tracks the URL of any pushed resource as per the host manifest. The diary has a fixed memory cost but can probabilistically store an unlimited number of items. As the diary fills up there is an increasing false negative rate, reducing its effectiveness. The trade-off is that more memory is spent per connection. Tune the bits per item and number of items to achieve optimal performance.

### `diaryBitsPerItem`

Default: `12`

Currently locked to `12` bits per item.

### `diaryTotalItems`

Default: `1024`

Set to `0` to disable the push diary.

The number of items to allocate space for in the server push diary.

## `sni`

Processing the Server Name Indication (SNI) domain name during the TLS handshake. The `domain` value is used by the `tls.loader` to retrieve TLS credentials.

### `sni.fallbackDomain`

Any invalid or missing SNI domain names are mapped to this fallback domain name.

Example: Use `localhost` as the default domain name.

```js
{
  sni: {
    fallbackDomain: 'example.com'
  }
}
```

### `sni.wildcards`

Array of objects mapping a domain name `suffix` to a `domain`. Used to look up a wildcard certificate for multiple subdomains.

Example: Look up the `*.commons.host` (including the asterisk symbol) domain for any sub-domains that end in `.commons.host`.

```js
{
  sni: {
    wildcards: [
      { suffix: '.commons.host',
        domain: '*.commons.host' }
    ]
  }
}
```

## `tls`

Dynamic retrieval of TLS credentials, i.e. public certificates, private keys, and certificate authority chains.

The domain name is provided by the SNI extension of TLS or a default fallback domain, as described in the `sni` settings.

### `loader`

The method to look up TLS credentials for a domain. Possible values:

- `'core'`: Use the Commons Host Core API. For operating as a CDN edge server. See the `core` settings.

- `'file'`: Look up using a file path. Used in local development or simple static web server.

#### `loader: 'core'`

Retrieve TLS private keys, public certificates, and certificate authority (CA) chains from the Core API.

#### `loader: 'file'`

Looks up files on the local file system based on templated file path strings and a file store path.

Example: Use default credentials if no ACME (e.g. Let's Encrypt) credentials are found.

```js
{
  tls: {
    loader: 'file',
    fallbackCa: [
      '/etc/tls/default/ca.intermediary.pem',
      '/etc/tls/default/ca.root.pem'
    ],
    fallbackCert: '/etc/tls/default/cert.pem',
    fallbackKey: '/etc/tls/default/key.pem',
    storePath: '/var/acme/certs',
    keyPath: '$store/$domain/key.pem',
    certPath: '$store/$domain/cert.pem'
  }
}
```

Any domains that do not have TLS credentials, as per `tls.keyPath` and `tls.certPath` lookups, are served using the fallback credentials in `tls.fallbackKey` and `tls.fallbackCert`.

If no fallback exists on launch, a self-signed certificate and key pair is generated using [tls-keygen](https://www.npmjs.com/package/tls-keygen). The key and certificate are stored in the current user's home directory, under the `~/.commonshost` hidden directory. The certificate is attempted to be added to to the operating system trusted store, which may require user confirmation and entry of their password. This is only done in CLI mode, or if the `generateCertificate` API argument is `true`; otherwise the server process exits.

### `tls.fallbackKey`

Default: `'~/.commonshost/key.pem'`

Loader: `'file'`

A string of the path to a PEM file containing the secret key.

### `tls.fallbackCert`

Default: `'~/.commonshost/cert.pem'`

Loader: `'file'`

A string of the path to a PEM file containing the public certificate.

### `tls.fallbackCa`

Default: `[]`

Loader: `'file'`

An array of strings of the paths to all files containing the certificate authority (CA) chain.

### `tls.storePath`

Default: `''`

Loader: `'file'`

Directory where keys and certificates are stored.

### `tls.keyPath`

Default: `'$store/$domain/key.pem'`

Loader: `'file'`

Path to the domain-specific key file.

May contain substitution variables `$store` and `$domain`.

### `tls.certPath`

Default: `'$store/$domain/cert.pem'`

Loader: `'file'`

Path to the domain-specific certificate file.

May contain substitution variables `$store` and `$domain`.

## `via`

The `Via` HTTP header is used to detect, and break out of, CDN loops. This can happen due to chained CDNs or misconfigured DNS.

### `via.pseudonym`

Default: `''`

By adding a token string to the `Via` HTTP header, intermediary proxies or CDN servers can identify themselves.

The `pseudonym` value should uniquely identify the intermediary. This is operationally dependent, for example in some cases multiple servers may form a single pool.

Example:

```js
via: {
  pseudonym: `${require('os').hostname()} (Commons Host)`
}
```

Response HTTP headers:

```
Via: 1.1 varnish, 2.0 us-nyc-3.commons.host (Commons Host), 1.1 s_bd39 (squid/3.5.23)
```

## `workers`

The server runs one master process that controls one or more worker processes. The workers process incoming requests and generate responses. Communication between the master and workers uses Node.js [cluster](https://nodejs.org/api/cluster.html) module message passing.

### `workers.count`

Default: `'max_physical_cpu_cores'`

The desired number of worker processes to spawn at launch. Must be a number or a [mathematical expression](https://www.npmjs.com/package/expr-eval) that evaluates to a number.

Examples:

Single worker process:

```js
workers: {
  count: 1
}
```

Invalid: must spawn at least 1 worker.

```js
workers: {
  count: 0
}
```

One worker per CPU core:

```js
workers: {
  count: 'max_physical_cpu_cores'
}
```

Half as many workers as CPU cores:

```js
workers: {
  count: 'max_physical_cpu_cores / 2'
}
```

Leave one core for other processes:

```js
workers: {
  count: 'max_physical_cpu_cores - 1'
}
```

## `www`

### `www.redirect`

Default: `false`

Enable to redirect all sites between the `www.` subdomain and root domain (also known as apex, bare, or naked domain).

Example: Given a scenario where only the hosts `example.net` and `www.example.com` are configured on the server.

| `www.redirect` | Request | Response |
|-|-|-|
| *any*   | example.net | `200` OK |
| *any*   | www.example.com | `200` OK |
| `true`  | www.example.net | `308` Redirect to example.net |
| `true`  | example.com | `308` Redirect to www.example.com |
| `false` | www.example.net | `404` Not Found |
| `false` | example.com | `404` Not Found |

## `hosts`

Default: `[]`

An array of objects containing [host options](#host-options) configurations.
