# Configuration

See [`@commonshost/configuration`](https://www.npmjs.com/package/@commonshost/configuration) for the JSON Schema, validation, and normalisation tools.

## Server Options

Applies globally to all sites on the server.

Details: [Server options reference](./server)

## Host Options

Applies to a single domain.

Details: [Host options reference](./host)

## Server Push Manifest

Also applies to a single domain, but defined as a separate specification.

Details: [HTTP/2 Server Push Manifest](/manifest)
