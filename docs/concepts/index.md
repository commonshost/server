# Concepts

## Server Push with Cache Digests

Page load time is a largely function of latency (round trip time × delays) and aggregate volume (number × size of assets).

Latency is minimised by using HTTP/2 Server Push to deliver any necessary assets to the browser alongside the HTML. When the browser parses the HTML it does not need to make a round trip request to fetch styles, scripts, and other assets. They are already in its cache or actively being pushed by the server as quickly as network conditions allow.

Volume is reduced by using strong compression (HPACK, Brotli, etc), and by avoiding sending redundant data.

If all assets were pushed every time, a large amount of bandwidth would be wasted. HTTP/1 asset concatenation makes a tradeoff between reducing round trips (good) and re-transferring invalidated, large files (bad). For example having to re-tranfer an entire spritesheet or JavaScript bundle because of one little change.

The HTTP/1 approach was to use file signatures (Etags) and timestamps to invalidate cached responses. This requires many expensive round trips where the browser checks with the server if any files have been modified.

Cache Digests to the rescue! Using a clever technique, called Golomb-Rice Coded Bloom Filters, a compressed list of cached responses is sent by the browser to the server. Now the server can avoid pushing assets that are fresh in the browser's cache.

With Server Push and Cache Digests the best practice is to have many small files that can be cached and updated atomically, instead of large, concatenated blobs.

Browsers do not yet support cache digests natively so Service Workers and the Cache API are used to implement them. A cookie-based fallback is available for browsers that lack Service Worker support.

## HTTPS Certificate

While the HTTP/2 specification allows unencrypted connections, web browsers strictly enforce HTTPS.

If no certificate and key are provided, one pair will be auto-generated. The generated certificate is only valid for `localhost`. It is stored in `~/.commonshost`. As a user convenience, the certificate is added as trusted to the operating system so browsers will accept the certificate. A password prompt may appear to confirm. This is currently only supported on macOS, Windows, and Linux.

In production use [Let's Encrypt](https://letsencrypt.org) or any other trusted, signed certificate.

Intermediate certificates are stapled to the OCSP response to speed up the TLS handshake.
