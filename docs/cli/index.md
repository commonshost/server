# CLI

```
./node_modules/.bin/server <command> [options]
```

Tip: In NPM `scripts` the relative path (`./node_modules/.bin/`) may be omitted. Looks neater.

## Default

Options that apply across all commands.

### `--version`, `-v`

Display the current version number.

### `--help [command]`, `-h [command]`

List global or command-specific options.

## Command: `start`

Start the server as a long running process that spawns workers.

Aliases: `run`, `up`, `launch`

### `$PORT`

The environment variable `PORT` sets the network port for incoming HTTPS connections.

The `https.port` and `http.to` options in the configuration file are set to `$PORT`.

The `http.from` port is derived by setting the last three decimal digits of `$PORT` set to `080` or `000`. E.g. HTTPS `8443` -> HTTP `8080`, HTTPS `443` -> HTTP `80`, HTTPS `10080` -> HTTP `10000`.

### `--options [file]`

Configuration file path. Defaults to: `./commons((.)host)(.conf(ig)).js(on)`

If no configuration file is found, the default configuration is used. See the [Configuration](#configuration) section for details.

The default configuration loads `./serverpush.json` as a server push manifest, if it exists. See [`@commonshost/manifest generate`](https://www.npmjs.com/package/@commonshost/manifest#generate-a-manifest) for details.

### `--watch`/`--no-watch`

Defaults to `false`, which is the same as specifying the `--no-watch` option.

The server maintains an in-memory index of all files. Compared to dynamic filesystem scanning, this dramatically improves performance when pushing many small files. However if files and directories are added, moved, or deleted, the index becomes incorrect. Pass the `--watch` option to monitor filesystem changes and automatically rebuild the index. This has some performance cost, depending on operating system specific support, so watching is disabled by default. Watching is typically only used in development mode, not production.

## Command: `stop`

Shuts down the server by sending it the `SIGINT` signal.

Aliases: `halt`, `exit`, `quit`, `kill`, `down`, `end`

## Command: `reload`

Gracefully restart workers with a new configuration by sending the server the `SIGHUP` signal.

Aliases: `restart`, `refresh`, `renew`, `update`, `cycle`, `load`

## Command: `test`

Loads and validates a server configuration.

Aliases: `verify`, `validate`, `check`, `configtest`, `lint`

The process prints `PASS` and exits `0` if the configuration is valid, or otherwise prints `FAIL` with an error message and exits `1`.

### `--options [file]`

Identical to the same option of the `start` command.
