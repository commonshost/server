const cluster = require('cluster')
const physicalCpuCount = require('physical-cpu-count')
const EventEmitter = require('events')
const { once } = require('events')
const { redirect } = require('./redirect')
const { watch } = require('chokidar')
const debounce = require('debounce-collect')
const recursiveReaddir = require('recursive-readdir')
const { load } = require('./configuration/load')
const { Parser } = require('expr-eval')
const { promisify } = require('util')
const pino = require('pino')
const { normaliseHost } = require('@commonshost/configuration')
const { randomBytes } = require('crypto')
const { tlsSniApiMaster } = require('./helpers/tlsSniApiMaster')

const timeout = (time) => new Promise((resolve) => setTimeout(resolve, time))

function evaluateToNumber (expression) {
  if (typeof expression === 'number') return expression
  return Parser.evaluate(
    expression,
    { max_physical_cpu_cores: physicalCpuCount }
  )
}

module.exports.Master = class Master extends EventEmitter {
  constructor (options) {
    super()
    this._options = options
    this.workers = []
    this.watchers = new Map()
    this.workersKeepalive = undefined
    this.refreshTicketKeys = undefined
    this.tlsSniApiHandler = undefined
  }

  async close () {
    if (this.redirector) {
      await promisify(this.redirector.close).call(this.redirector)
    }
    if (this.gopher) {
      await this.gopher.stop()
    }
    clearInterval(this.workersKeepalive)
    clearInterval(this.refreshTicketKeys)
    for (const worker of this.workers) {
      worker.kill()
      await once(worker, 'exit')
    }
    this.workers = []
    for (const [, watcher] of this.watchers) {
      watcher.close()
    }
    this.watchers.clear()
    await timeout(100)
  }

  async listen () {
    await this._setup()
    await this._startWorkers()
    await this._startRedirector()
  }

  async reload () {
    await this._setup()
    const retirees = this.workers
    await this._startWorkers()
    for (const retiree of retirees) {
      retiree.kill()
    }
    await this._startRedirector()
  }

  async message (message) {
    if (!('root' in message && 'type' in message && 'domain' in message)) {
      this.log.warn('Invalid message: Must have all required properties.')
      return
    }
    if (
      (message.type === 'site-deploy' && message.hasNewFiles === true) ||
      message.type === 'file-delete' ||
      message.type === 'file-update'
    ) {
      message.index = await recursiveReaddir(message.root)
    }
    if ('configuration' in message) {
      const { configuration: host } = message
      const options = { externalManifest: false }
      message.configuration = await normaliseHost(host, options)
    }
    for (const worker of this.workers) {
      worker.send(message)
    }
    this.emit('message', message, this)
  }

  async _setup () {
    this.options = await load(this._options)
    this.log = pino(this.options.log)

    if (this._options.watch === true) {
      for (const [, watcher] of this.watchers) {
        watcher.close()
      }
      this.watchers.clear()
      for (const { root, domain } of this.options.hosts) {
        const watcher = watch(root, {
          ignored: /(^|[/\\])\../,
          ignoreInitial: true,
          followSymlinks: false,
          awaitWriteFinish: {
            stabilityThreshold: 1000,
            pollInterval: 500
          }
        })
        watcher.on('all', debounce(async (events) => {
          const updated = new Set()
          const deleted = new Set()
          for (const [type, path] of events) {
            if (type === 'add' || type === 'change') {
              updated.add(path)
            } else if (type === 'unlink') {
              deleted.add(path)
            }
          }
          if (updated.size > 0) {
            await this.message({
              root,
              domain,
              type: 'file-update',
              files: Array.from(updated)
            })
          }
          if (deleted.size > 0) {
            await this.message({
              root,
              domain,
              type: 'file-delete',
              files: Array.from(deleted)
            })
          }
        }, 1000))
        this.watchers.set(root, watcher)
      }
    }
  }

  async _startRedirector () {
    if (this.redirector) {
      this.redirector.close()
      delete this.redirector
    }
    if (this.options.http.redirect === true) {
      this.redirector = await redirect(this.options)
    }
  }

  async _startWorkers () {
    const files = {}
    for (const { root, domain } of this.options.hosts) {
      try {
        // Scan files so workers can build a cached lookup index.
        // See also: hostOptions middleware
        files[domain] = { root, index: await recursiveReaddir(root) }
      } catch (error) {
        this.log.error(error)
        files[domain] = { root, index: [] }
      }
    }

    const workers = []
    const maxWorkerCount = Math.ceil(
      evaluateToNumber(this.options.workers.count)
    )
    if (maxWorkerCount < 1) {
      throw new Error(`Workers count ${maxWorkerCount} must be at least 1`)
    }
    cluster.setupMaster({
      args: [
        '--heapsnapshot-signal', 'SIGSEGV',
        ...process.argv.slice(2)
      ],
      exec: require.resolve('./worker.js')
    })
    if (!this.tlsSniApiHandler) {
      this.tlsSniApiHandler = tlsSniApiMaster(this)
    }
    while (workers.length < maxWorkerCount) {
      const worker = cluster.fork()
      workers.push(worker)
      const relayMessage = (message) => {
        this.emit('worker-message', message, worker)
      }
      worker.on('message', relayMessage)
      worker.once('exit', (code, signal) => {
        worker.off('message', relayMessage)
        if (code) {
          this.log.error(`Worker exit: code ${code}, signal ${signal}`)
          process.exit(code)
        }
      })
    }
    await Promise.all(workers.map((worker) => once(worker, 'online')))
    const message = {
      type: 'start',
      files,
      options: this.options,
      ticketKeys: randomBytes(48).toString('hex')
    }
    for (const worker of workers) {
      worker.send(message)
    }
    await Promise.all(workers.map((worker) => once(worker, 'listening')))
    this.workers = workers

    this.workersKeepalive = setInterval(() => {
      for (const worker of this.workers) {
        worker.send({ type: 'keepalive' })
      }
    }, 10 * 1000)

    this.refreshTicketKeys = setInterval(() => {
      const message = {
        type: 'ticket-keys-refresh',
        ticketKeys: randomBytes(48).toString('hex')
      }
      for (const worker of this.workers) {
        worker.send(message)
      }
    }, 60 * 60 * 1000)

    for (const { root, domain } of this.options.hosts) {
      const authority = this.options.https.port === 443
        ? domain : `${domain}:${this.options.https.port}`
      this.log.info(`Serving ${root} on https://${authority}`)
    }
  }
}
