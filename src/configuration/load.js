const { join, resolve, relative } = require('path')
const { fileExists } = require('../helpers/fileExists')
const { directoryExists } = require('../helpers/directoryExists')
const defaultsDeep = require('lodash.defaultsdeep')
const { defaultOptions } = require('./default/options')
const {
  validate,
  normaliseHost,
  loadFile,
  findFile
} = require('@commonshost/configuration')
const { keygen } = require('tls-keygen')
const userHome = require('user-home')
const pino = require('pino')
const { staticDirectories } = require('@commonshost/static-directories')

async function load ({
  options = undefined,
  generateCertificate = false,
  serveDefaultSite = true,
  cwd = process.cwd(),
  configurationFilepath
} = {}) {
  const log = pino(options && options.log)
  let userOptions
  let defaultManifest
  if (typeof options === 'object') {
    userOptions = options
  } else if (typeof options === 'string' && options.length > 0) {
    const filepath = resolve(cwd, options)
    log.info(`Given configuration file: ${relative(cwd, filepath)}`)
    userOptions = await loadFile(filepath)
    configurationFilepath = filepath
  } else if (typeof options === 'undefined') {
    const filepath = await findFile(cwd)
    if (filepath !== undefined) {
      log.info(`Found configuration file: ${relative(cwd, filepath)}`)
      userOptions = await loadFile(filepath)
      configurationFilepath = filepath
    }
    if (userOptions === undefined) {
      userOptions = {}
      log.warn('Loading default server configuration')
      const manifest = 'serverpush.json'
      if (fileExists(resolve(cwd, manifest))) {
        defaultManifest = manifest
        configurationFilepath = join(cwd, 'configuration')
        log.warn(`Found server push manifest: ${relative(cwd, manifest)}`)
      }
    }
  } else {
    throw new Error('Invalid options type supplied')
  }

  const result = defaultsDeep(userOptions, defaultOptions)

  if (result.tls.loader === 'file') {
    result.tls = defaultsDeep(result.tls, {
      certPath: '$store/$domain/cert.pem',
      keyPath: '$store/$domain/key.pem',
      storePath: '',
      fallbackCa: [],
      fallbackCert: '',
      fallbackKey: ''
    })
  }

  if (serveDefaultSite === true && result.hosts.length === 0) {
    const host = await normaliseHost()
    if (defaultManifest) {
      host.manifest = defaultManifest
    }
    result.hosts.push(host)
  }

  result.hosts = await Promise.all(result.hosts.map(async (host) => {
    let { domain, root } = host

    if (!domain) {
      domain = 'localhost'
    }

    if (root) {
      root = resolve(cwd, root)
    } else {
      for (const directory of staticDirectories) {
        const destination = join(cwd, directory)
        if (directoryExists(destination)) {
          root = destination
          log.info(`Static site found: ${relative(cwd, root)}`)
          break
        }
      }
      if (!root) {
        root = cwd
        log.info(`Defaulting to current directory: ${root}`)
      }
    }

    const overrides = { domain, root }
    const cloned = Object.assign({}, host, overrides)
    const options = { configurationFilepath, externalManifest: true }
    const normalised = await normaliseHost(cloned, options)
    return normalised
  }))

  if (process.env.PORT !== undefined) {
    const httpsPort = Number(process.env.PORT)
    result.https.port = httpsPort
    result.http.to = httpsPort
    const roundedThousand = httpsPort - (httpsPort % 1000)
    result.http.from = roundedThousand + 80
    if (result.http.from === result.https.port) {
      result.http.from = roundedThousand
    }
  }

  if (result.acme.redirect.endsWith('/')) {
    result.acme.redirect = result.acme.redirect.replace(/\/+$/g, '')
  }

  if (result.acme.webroot !== '') {
    result.acme.webroot = resolve(cwd, result.acme.webroot)
  }

  if (result.tls.loader === 'file') {
    if (result.tls.storePath !== '') {
      result.tls.storePath = resolve(cwd, result.tls.storePath)
    }

    if (result.placeholder.hostNotFound !== '') {
      result.placeholder.hostNotFound =
        resolve(cwd, result.placeholder.hostNotFound)
    }

    if (result.tls.fallbackKey === '' && result.tls.fallbackCert === '') {
      const key = join(userHome, '.commonshost/key.pem')
      const cert = join(userHome, '.commonshost/cert.pem')
      if (fileExists(key) && fileExists(cert)) {
        result.tls.fallbackKey = key
        result.tls.fallbackCert = cert
      } else if (generateCertificate === true) {
        log.info(
          '🔐 Generating a private TLS certificate.\n' +
          '   Confirm to add as a trusted certificate to your key chain.'
        )
        await keygen({ key, cert })
        result.tls.fallbackKey = key
        result.tls.fallbackCert = cert
      } else {
        throw new Error('Missing a private key & public certificate pair.')
      }
    }
  }

  validate(result)

  return result
}

module.exports.load = load
