module.exports.defaultOptions = {
  signature: true,
  via: {
    pseudonym: ''
  },
  acme: {
    proxy: '',
    redirect: '',
    webroot: ''
  },
  doh: false,
  goh: false,
  gopher: false,
  placeholder: {
    hostNotFound: ''
  },
  log: {
    level: 'info'
  },
  workers: {
    count: 'max_physical_cpu_cores'
  },
  http: {
    redirect: true,
    from: 8080,
    to: 8443
  },
  http2: {
    timeout: null
  },
  https: {
    port: 8443
  },
  push: {
    diaryBitsPerItem: 12,
    diaryTotalItems: 1024
  },
  sni: {
    fallbackDomain: '',
    wildcards: []
  },
  tls: {
    loader: 'file'
  },
  www: {
    redirect: false
  },
  hosts: []
}
