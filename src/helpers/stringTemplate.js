module.exports.stringTemplate =
function stringTemplate (string, parameters) {
  if (typeof string !== 'string') {
    throw new TypeError(`First argument must be a string. Value: ${string}`)
  }
  return string.replace(
    /\$(\w+)/g,
    (match, key) => key in parameters ? parameters[key] : match
  )
}
