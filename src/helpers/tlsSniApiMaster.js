const fetch = require('node-fetch')
const { getAccessToken } = require('./getAccessToken')
const LRU = require('lru-cache')

module.exports.tlsSniApiMaster = (server) => {
  const responseCache = new LRU({
    max: 1000,
    maxAge: 24 * 60 * 60 * 1000
  })
  const apiRequests = new Map()

  server.on('master-message', ({ type, domain }) => {
    if (type === 'certificate-issue' ||
      type === 'certificate-revoke' ||
      type === 'site-delete'
    ) {
      responseCache.del(domain)
    }
  })

  server.on('worker-message', async ({ type, servername }, worker) => {
    if (type === 'tls-credentials-request') {
      let message = responseCache.get(servername)
      if (message) {
        worker.send(message)
        return
      }
      if (apiRequests.has(servername)) {
        apiRequests.get(servername).add(worker)
        return
      }
      apiRequests.set(servername, new Set([worker]))
      const domain = encodeURIComponent(servername)
      const url = `${server.options.core.origin}/v2/sites/${domain}/certificate`
      try {
        const headers = {
          'authorization': `Bearer ${await getAccessToken(server.options)}`,
          'accept': 'application/json'
        }
        const response = await fetch(url, { headers })
        const { key, cert, ca } = await response.json()
        if (!key || !cert) throw new Error('Missing TLS credentials')
        message = { type: 'tls-credentials-response', servername, key, cert, ca }
      } catch (error) {
        message = { type: 'tls-credentials-response', servername, error }
      } finally {
        const workers = apiRequests.get(servername)
        apiRequests.delete(servername)
        responseCache.set(servername, message)
        for (const worker of workers) {
          worker.send(message)
        }
      }
    }
  })
}
