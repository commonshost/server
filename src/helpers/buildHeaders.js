const mime = require('mime')
const compressible = require('compressible')
const UriTemplate = require('uri-templates')

const compressors = [
  { extension: '.br', encoding: 'br' },
  { extension: '.gz', encoding: 'gzip' },
  { extension: '.deflate', encoding: 'deflate' }
]

const variableRegex = /\{([a-zA-Z]+)\}/g

const headerCharRegex = /[^\t\x20-\x7e\x80-\xff]/
/**
 * True if val contains an invalid field-vchar
 *  field-value    = *( field-content / obs-fold )
 *  field-content  = field-vchar [ 1*( SP / HTAB ) field-vchar ]
 *  field-vchar    = VCHAR / obs-text
 */
function checkInvalidHeaderChar (val) {
  return headerCharRegex.test(val)
}

module.exports.buildHeaders = function buildHeaders (
  sourceFile,
  urlPath,
  supportedEncodings,
  fileIndex,
  options
) {
  let resolved = sourceFile
  const headers = {}

  const type = mime.getType(sourceFile.absolute)
  if (type !== null) {
    headers['content-type'] = type.startsWith('text/')
      ? `${type}; charset=utf-8`
      : type
  }

  headers['cache-control'] = 'public, max-age=1, must-revalidate'

  if (compressible(type)) {
    for (const { extension, encoding } of compressors) {
      const variant = sourceFile.absolute + extension
      if (fileIndex.absolute.has(variant) &&
        supportedEncodings.includes(encoding)
      ) {
        headers['content-encoding'] = encoding
        resolved = fileIndex.absolute.get(variant)
        break
      }
    }
  }

  if (options !== undefined) {
    for (const { uri, fields } of options.headers) {
      if (uri) {
        const predicate = new UriTemplate(uri)
        const parts = predicate.fromUri(urlPath)
        const replacer = (match, key) => parts[key]
        if (parts !== undefined) {
          for (const [name, value] of Object.entries(fields)) {
            const substitution = Array.isArray(value)
              ? value.map((part) => value.replace(variableRegex, replacer))
              : value.replace(variableRegex, replacer)
            if (!checkInvalidHeaderChar(substitution)) {
              headers[name] = substitution
            }
          }
        }
      } else {
        for (const [name, value] of Object.entries(fields)) {
          if (!checkInvalidHeaderChar(value)) {
            headers[name] = value
          }
        }
      }
    }
  }

  return { headers, resolved }
}
