const { createSecureContext } = require('tls')
const LRU = require('lru-cache')
const { resolve } = require('path')
const { promises: { readFile } } = require('fs')
const { stringTemplate } = require('./stringTemplate')

module.exports.tlsSniFile = (options) => {
  const tlsCache = new LRU({ max: 100 })

  process.on('message', ({ type, domain }) => {
    if (type === 'certificate-issue' ||
      type === 'certificate-revoke' ||
      type === 'site-delete'
    ) {
      tlsCache.del(domain)
    }
  })

  return async function getSecureContext (servername) {
    let secureContext = tlsCache.get(servername)
    if (!secureContext) {
      try {
        if (!servername) throw new Error('Missing SNI servername')
        const parameters = { store: options.tls.storePath, domain: servername }
        const keyPath = stringTemplate(options.tls.keyPath, parameters)
        const certPath = stringTemplate(options.tls.certPath, parameters)
        const [key, cert] = await Promise.all([
          readFile(resolve(options.tls.storePath, keyPath)),
          readFile(resolve(options.tls.storePath, certPath))
        ])
        secureContext = createSecureContext({ key, cert })
      } catch (error) {
        try {
          const [key, cert, ca] = await Promise.all([
            readFile(options.tls.fallbackKey),
            readFile(options.tls.fallbackCert),
            options.tls.fallbackCa.filter(String).map(readFile)
          ])
          secureContext = createSecureContext({ key, cert, ca })
        } catch (error) {
          throw error
        }
      }
      tlsCache.set(servername, secureContext)
    }
    return secureContext
  }
}
