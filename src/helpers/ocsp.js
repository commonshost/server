const ocsp = require('ocsp')

const ocspCache = new ocsp.Cache()

module.exports.OCSPRequestHandler =
function OCSPRequestHandler (certificate, issuer, callback) {
  if (!issuer) return callback()
  ocsp.getOCSPURI(certificate, (error, uri) => {
    if (error) return callback(error)
    if (uri === null) return callback()
    const request = ocsp.request.generate(certificate, issuer)
    ocspCache.probe(request.id, (error, { response }) => {
      if (error) return callback(error)
      if (response) return callback(null, response)
      const options = {
        url: uri,
        ocsp: request.data
      }
      ocspCache.request(request.id, options, callback)
    })
  })
}
