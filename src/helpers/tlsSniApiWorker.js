const { createSecureContext } = require('tls')
const LRU = require('lru-cache')

module.exports.tlsSniApiWorker = (options) => {
  const tlsCache = new LRU({
    max: 100,
    maxAge: 1 * 60 * 60 * 1000
  })
  const tlsRequests = new Map()

  process.on('message', ({ type, domain }) => {
    if (type === 'certificate-issue' ||
      type === 'certificate-revoke' ||
      type === 'site-delete'
    ) {
      tlsCache.del(domain)
    }
  })

  return async function getSecureContext (servername) {
    let secureContext = tlsCache.get(servername)
    if (!secureContext) {
      if (tlsRequests.has(servername)) {
        return tlsRequests.get(servername)
      } else {
        const lookup = new Promise((resolve, reject) => {
          process.on('message', function onMessage (message) {
            if (
              message.type === 'tls-credentials-response' &&
              message.servername === servername
            ) {
              process.off('message', onMessage)
              tlsRequests.delete(servername)
              try {
                if ('error' in message) {
                  throw message.error
                } else {
                  const secureContext = createSecureContext(message)
                  tlsCache.set(servername, secureContext)
                  resolve(secureContext)
                }
              } catch (error) {
                reject(error)
              }
            }
          })
        })
        process.send({ type: 'tls-credentials-request', servername })
        tlsRequests.set(servername, lookup)
        return lookup
      }
    }
    return secureContext
  }
}
