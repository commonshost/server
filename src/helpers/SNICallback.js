const isDomainName = require('is-domain-name')
const isIp = require('is-ip')
const { tlsSniApiWorker } = require('./tlsSniApiWorker')
const { tlsSniFile } = require('./tlsSniFile')

function getCommonname (servername, options) {
  if (
    typeof servername !== 'string' ||
    servername.includes('..') ||
    !isDomainName(servername) ||
    isIp(servername)
  ) {
    return options.sni.fallbackDomain
  }
  for (const { suffix, domain } of options.sni.wildcards) {
    if (servername.endsWith(suffix)) {
      return domain
    }
  }
  return servername
}

function getLoader (loader) {
  switch (loader) {
    case 'core':
      return tlsSniApiWorker
    case 'file':
      return tlsSniFile
    default:
      throw new Error('Invalid TLS loader')
  }
}

module.exports.SNICallback =
function SNICallback (options) {
  const getSecureContext = getLoader(options.tls.loader)(options)
  return async function SNICallback (servername, callback) {
    const commonname = getCommonname(servername, options)
    let secureContext
    try {
      secureContext = await getSecureContext(commonname)
    } catch (error) {
      return callback(error)
    }
    return callback(null, secureContext)
  }
}
