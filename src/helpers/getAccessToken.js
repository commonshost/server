const fetch = require('node-fetch')

let expiration = 0
let token

module.exports.getAccessToken =
async function getAccessToken ({ core: { auth0: options } }) {
  if (options.accessToken) return options.accessToken
  if (Date.now() < expiration) return token
  const url = options.issuer + 'oauth/token'
  const response = await fetch(url, {
    method: 'POST',
    headers: { 'content-type': 'application/json' },
    body: JSON.stringify({
      grant_type: 'client_credentials',
      audience: options.audience,
      scope: ['global_read'].join(' '),
      client_id: options.clientId,
      client_secret: options.clientSecret
    })
  })
  const data = await response.json()
  if (!('access_token' in data)) {
    throw new Error(`Failed to get a token: ${data.error_description}`)
  }
  token = data.access_token
  expiration = Date.now() + data.expires_in
  return token
}
