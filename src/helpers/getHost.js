module.exports.getHost = (request) => {
  return request.headers['x-forwarded-host'] ||
    request.headers[':authority'] ||
    request.headers['host'] ||
    ''
}
