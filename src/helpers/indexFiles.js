const { dirname, basename } = require('path')

const compressedEncodings = ['.br', '.gz', '.deflate']

function isAlternativeEncoding (filepath, filepaths) {
  for (const extension of compressedEncodings) {
    if (filepath.endsWith(extension)) {
      const { length } = extension
      const uncompressed = filepath.slice(0, -length)
      if (filepaths.has(uncompressed)) {
        return true
      }
    }
  }
  return false
}

module.exports.indexFiles =
function indexFiles (root, index, trailingSlash) {
  const filepaths = new Set(index)
  const absolute = new Map()
  const relative = new Map()
  const pathnames = new Map()
  for (const filepath of filepaths) {
    const asAbsolute = filepath.replace(/[\\/]+/g, '/')
    const entry = {
      absolute: asAbsolute
    }
    absolute.set(asAbsolute, entry)
    if (!isAlternativeEncoding(filepath, filepaths)) {
      const asRelative = asAbsolute.substr(root.length)
      entry.relative = asRelative
      relative.set(asRelative, entry)

      let asPathname
      if (basename(asRelative) === 'index.html') {
        if (trailingSlash === 'always' && asRelative !== '/index.html') {
          asPathname = `${dirname(asRelative)}/`
        } else {
          asPathname = dirname(asRelative)
        }
      } else {
        asPathname = asRelative
      }
      asPathname = encodeURI(asPathname)
      entry.pathname = asPathname
      pathnames.set(asPathname, entry)
      if (asPathname !== asRelative) {
        pathnames.set(asRelative, entry)
      }
    }
  }
  return { relative, absolute, pathnames }
}
