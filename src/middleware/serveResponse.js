const Negotiator = require('negotiator')
const { createReadStream, stat } = require('fs')
const { promisify } = require('util')
const { buildHeaders } = require('../helpers/buildHeaders')
const { serveDependencies } = require('./serveDependencies')
const { NotFound, InternalServerError } = require('http-errors')

module.exports.serveResponse = () => {
  return async function serveResponse (request, response, next) {
    const { resolved, headers } = buildHeaders(
      request.resolved,
      request.url,
      new Negotiator(request).encodings(),
      request.fileIndex,
      request.options
    )

    const statusCode = response.statusCode || 200

    if (request.httpVersionMajor === 2) {
      request.once('error', next)
      response.once('error', next)
      response.stream.once('error', next)
      if (request.method === 'HEAD') {
        try {
          response.setHeader(
            'content-length',
            (await promisify(stat)(resolved.absolute)).size
          )
        } catch (error) {
          return next(error)
        }
        response.writeHead(statusCode, headers)
        response.end()
      } else {
        headers[':status'] = statusCode
        response.stream.respondWithFile(
          resolved.absolute,
          Object.assign(response.getHeaders(), headers),
          {
            statCheck (stat, headers) {
              if (request.method === 'HEAD') {
                headers['content-length'] = stat.size
                response.stream.respond(headers)
                return false
              }
            },
            onError ({ code }) {
              if (code === 'ENOENT') {
                next(new NotFound())
              } else {
                next(new InternalServerError())
              }
            }
          }
        )
        try {
          await serveDependencies(request, response)
        } catch (error) {
          request.log.error(error)
        }
      }
    } else {
      try {
        response.setHeader(
          'content-length',
          (await promisify(stat)(resolved.absolute)).size
        )
      } catch (error) {
        return next(error)
      }
      if (request.method === 'HEAD') {
        response.writeHead(statusCode, headers)
        response.flushHeaders()
        response.end()
      } else {
        const file = createReadStream(resolved.absolute)
        file.once('open', async () => {
          response.writeHead(statusCode, headers)
          file.pipe(response)
        })
        file.once('error', (error) => {
          if (!response.headersSent) next(error)
          else if (!response.finished) response.end()
        })
      }
    }
  }
}
