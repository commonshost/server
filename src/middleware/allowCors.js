module.exports.allowCors = () => {
  return function allowCors (request, response, next) {
    const allowed = request.options.accessControl.allowOrigin
    if (allowed) {
      response.setHeader('access-control-allow-origin', allowed)
    }
    if (request.method === 'OPTIONS') {
      response.flushHeaders()
      response.end()
      return
    }
    next()
  }
}
