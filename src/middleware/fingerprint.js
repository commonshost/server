const { sync: readPkgUp } = require('read-pkg-up')
const osName = require('os-name')

function serverHeader () {
  const { pkg } = readPkgUp({ cwd: __dirname })
  const { version } = pkg
  const { node } = process.versions
  const products = [
    `commonshost ${version}`,
    `nodejs ${node}`,
    `${osName()}`
  ]
  return products.join(', ')
}
const HEADER_SERVER = serverHeader()

module.exports.fingerprint = ({ signature, via }) => {
  return function fingerprint (request, response, next) {
    if (signature === true) {
      response.setHeader('server', HEADER_SERVER)
    }

    if (via.pseudonym) {
      const intermediaries = request.headers['via']
      const intermediary = `${request.httpVersion} ${via.pseudonym}`
      const field = intermediaries === undefined
        ? intermediary
        : `${intermediaries}, ${intermediary}`
      response.setHeader('via', field)
    }

    next()
  }
}
