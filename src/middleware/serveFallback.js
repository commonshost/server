const { serveResponse } = require('./serveResponse')
const accepts = require('accepts')
const parseUrl = require('parseurl')
const { extname } = require('path')

module.exports.serveFallback = (options) => {
  const passthrough = serveResponse(options)
  return function serveFallback (error, request, response, next) {
    if (request.options === undefined) {
      if (parseInt(error.statusCode) === 404 && options.placeholder.hostNotFound) {
        const accepted = accepts(request)
        const url = parseUrl(request)
        const extension = extname(url.pathname)
        if (accepted.type('text/html') &&
          (extension === '' || extension === '.html')
        ) {
          response.statusCode = 404
          request.resolved = {
            absolute: options.placeholder.hostNotFound,
            pathname: encodeURI(url.pathname)
          }
          request.fileIndex = { absolute: new Map() }
          passthrough(request, response, next)
          return
        }
      }
    } else {
      const fallback = request.options.fallback[error.statusCode]
      if (fallback !== undefined) {
        const { fileIndex: { absolute } } = request
        if (absolute.has(fallback)) {
          response.statusCode = parseInt(error.statusCode)
          request.resolved = absolute.get(fallback)
          passthrough(request, response, next)
          return
        }
      }
    }
    next(error)
  }
}
