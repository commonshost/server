const { getDependencies } = require('../helpers/getDependencies')
const { getOrigin } = require('../helpers/getOrigin')
const { requestDestination } = require('request-destination')
const CuckooFilter = require('cuckoofilter-native')

const HTTP2_PRIORITY_DEFAULT = 16

const diaries = new WeakMap()

function resolveDependencies (options) {
  const {
    // diaryBitsPerItem,
    diaryTotalItems
  } = options.push

  function getDiary (session) {
    if (diaries.has(session)) {
      return diaries.get(session)
    } else {
      const diary = new CuckooFilter(diaryTotalItems /* , diaryBitsPerItem */)
      diaries.set(session, diary)
      return diary
    }
  }

  return async function resolveDependencies (request, response, next) {
    if ((request.httpVersionMajor === 2 && !response.stream.pushAllowed) ||
      request.options.manifest.length === 0 ||
      diaryTotalItems === 0
    ) {
      return next()
    }

    const baseUrl = getOrigin(request)
    request.pushResponses = []

    const dependencies = getDependencies(
      request.options.manifest,
      request.fileIndex.relative,
      request.resolved.relative
    )

    if (request.httpVersionMajor === 2 && response.stream.pushAllowed) {
      const headers = {
        ':method': request.method,
        ':scheme': 'https',
        ':authority': request.authority
      }
      const { stream } = response
      const pushPromises = []
      for (const [relative, priority] of dependencies) {
        const dependency = request.fileIndex.relative.get(relative)
        const { pathname } = dependency
        const url = baseUrl + pathname

        let diary = getDiary(stream.session)
        if (diary.contain(url)) {
          continue
        } else if (diary.size < diaryTotalItems) {
          try { diary.add(url) } catch (error) {}
        }

        pushPromises.push(new Promise((resolve, reject) => {
          response.log.info(`PUSH ${url}`)
          headers[':path'] = pathname
          stream.pushStream(headers, (error, stream) => {
            if (error) return reject(error)
            else resolve({ pushResponse: stream, dependency })
          })
          if (priority !== HTTP2_PRIORITY_DEFAULT) {
            stream.priority({ weight: priority, silent: true })
          }
        }))
      }
      request.pushResponses = await Promise.all(pushPromises)
    } else if (request.httpVersionMajor === 1) {
      const links = []
      for (const [relative] of dependencies) {
        const { pathname } = request.fileIndex.relative.get(relative)
        const destination = requestDestination(relative)
        links.push(`<${pathname}>; rel=preload; as=${destination}`)
      }
      response.setHeader('link', links)
    }

    next()
  }
}

module.exports.resolveDependencies = resolveDependencies
