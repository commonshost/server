const { LoopDetected } = require('http-errors')

module.exports.cdnLoopPrevention = ({ pseudonym } = {}) => {
  return function cdnLoopPrevention (request, response, next) {
    if (pseudonym) {
      const via = request.headers['via']
      if (via !== undefined) {
        const intermediary = `${request.httpVersion} ${pseudonym}`
        if (via.includes(intermediary)) {
          next(new LoopDetected())
          return
        }
      }
    }
    next()
  }
}
