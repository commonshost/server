const { untilBefore } = require('until-before')
const { getHost } = require('../helpers/getHost')
const { BadRequest } = require('http-errors')
const { redirect } = require('../helpers/redirect')

module.exports.redirectHttps = (options) => {
  return async function redirectHttps (request, response, next) {
    const host = getHost(request)
    const hostname = untilBefore.call(host, ':')
    if (hostname === '') {
      return next(new BadRequest())
    } else {
      return redirect(response, hostname, options.http.to, request.url)
    }
  }
}
