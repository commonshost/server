require('hard-rejection/register')
const { server } = require('./server')

const { Gopher } = require('./gopher')
const { SNICallback } = require('./helpers/SNICallback')
const { OCSPRequestHandler } = require('./helpers/ocsp')

process.title = 'commonshost-worker'

async function onMessage ({ type, ticketKeys, options, files }) {
  if (type === 'start') {
    process.off('message', onMessage)
    const app = server(ticketKeys, options, files)
    app.listen(options.https.port)

    if (typeof options.gopher === 'object') {
      const gopher = new Gopher({
        ...options.gopher,
        SNICallback: SNICallback(options),
        OCSPRequestHandler,
        ticketKeys: Buffer.from(ticketKeys, 'hex')
      })
      await gopher.start()
      process.on('message', ({ type, ticketKeys }) => {
        if (type === 'ticket-keys-refresh') {
          gopher.server.setTicketKeys(Buffer.from(ticketKeys, 'hex'))
        }
      })
    }
  }
}

process.on('message', onMessage)
