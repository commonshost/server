const { read, unlock, matchByName } = require('../helpers/pid')

module.exports.command = ['stop']
module.exports.aliases = ['halt', 'exit', 'quit', 'kill', 'down', 'end']
module.exports.desc = 'Stop the server'
module.exports.builder = {}

module.exports.handler = async (argv) => {
  const pidByFile = read()
  if (!isNaN(pidByFile)) {
    const pidByName = await matchByName(pidByFile)
    if (pidByName === pidByFile) {
      console.log(`Stopping process ${pidByFile}`)
      process.kill(pidByFile, 'SIGINT')
    } else {
      console.log('Removing stale lock')
      unlock()
    }
  } else {
    console.log('No active process found')
  }
}
