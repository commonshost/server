const { load } = require('../configuration/load')

module.exports.command = ['test']
module.exports.aliases = ['verify', 'validate', 'check', 'configtest', 'lint']
module.exports.desc = 'Check the configuration for problems'
module.exports.builder = {
  options: {
    type: 'string',
    describe: 'Configuration file path'
  }
}

module.exports.handler = async ({ options }) => {
  try {
    await load({ options })
    console.log('PASS')
  } catch (error) {
    console.error(`FAIL: ${error.message}`)
    process.exit(1)
  }
}
