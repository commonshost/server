const { read, matchByName } = require('../helpers/pid')

module.exports.command = ['reload']
module.exports.aliases = ['restart', 'refresh', 'renew', 'update', 'cycle', 'load']
module.exports.desc = 'Gracefully restart workers with a new configuration'
module.exports.builder = {}

module.exports.handler = async (argv) => {
  const pid = read()
  if (!isNaN(pid) && pid === await matchByName(pid)) {
    console.log(`Reloading process ${pid}`)
    process.kill(pid, 'SIGHUP')
  } else {
    console.log('No active process found')
  }
}
