const connect = require('connect')
const { fingerprint } = require('./middleware/fingerprint')
const { logger } = require('./middleware/logger')
const { playdoh } = require('playdoh')
const { goh } = require('goh')
const { hostOptions } = require('./middleware/hostOptions')
const { allowedMethods } = require('./middleware/allowedMethods')
const { allowCors } = require('./middleware/allowCors')
const { cdnLoopPrevention } = require('./middleware/cdnLoopPrevention')
const { resolveRequest } = require('./middleware/resolveRequest')
const { resolveDependencies } = require('./middleware/resolveDependencies')
const { serveResponse } = require('./middleware/serveResponse')
const { serveFallback } = require('./middleware/serveFallback')
const { errorHandler } = require('middleware-plain-error-handler')

module.exports.app = (options, files) => {
  const app = connect()
  app.use(fingerprint(options))
  app.use(logger(options.log))
  app.use(hostOptions(options, files))
  app.use(allowCors())
  app.use(cdnLoopPrevention(options.via))
  if (typeof options.doh === 'object') {
    app.use(playdoh(options.doh))
  }
  app.use(allowedMethods(['GET', 'HEAD', 'OPTIONS']))
  if (typeof options.goh === 'object') {
    app.use(goh(options.goh))
  }
  app.use(resolveRequest())
  app.use(resolveDependencies(options))
  app.use(serveResponse(options))
  app.use(serveFallback(options))
  app.use(errorHandler())
  return app
}
