const { GopherServer } = require('goth')
const { resolve, join } = require('path')
const { createReadStream } = require('fs')
const { stringTemplate } = require('./helpers/stringTemplate')
const { once } = require('events')

const GOPHER_PORT = 70

class Gopher {
  constructor ({
    plaintextFallback = '',
    port = GOPHER_PORT,
    secureRoot = 'sites/$domain/public/_gopher',
    SNICallback,
    ticketKeys
  }) {
    this.port = port
    this.secureRoot = secureRoot
    this.plaintextFallback = plaintextFallback
    this.server = new GopherServer({ SNICallback, ticketKeys })
    this.server.on('gopherConnection', this.onConnection.bind(this))
    this.server.on('connection', (socket) => {
      socket.setTimeout(10000, () => socket.end())
    })
  }

  async start () {
    this.server.listen(this.port)
    await once(this.server, 'listening')
  }

  async stop () {
    this.server.close()
    await once(this.server, 'close')
  }

  async onConnection (socket, type) {
    socket.on('error', (error) => {
      if (error.code !== 'EPIPE') {
        console.error(error)
      }
    })
    if (type !== 'tls' || !socket.servername) {
      try {
        await serveFile(socket, this.plaintextFallback)
      } catch (error) {
        if (error.code === 'EISDIR' || error.code === 'ENOENT') {
          socket.end('3Secure connection required\t\terror.host\t1\r\n.\r\n')
        }
      }
      socket.destroy()
      return
    }
    if (socket.servername.includes('..')) {
      socket.destroy()
      return
    }
    socket.once('data', async (chunk) => {
      const selector = resolve('/', chunk.toString().split(/[\t\r\n]/, 1)[0])
      const root = stringTemplate(this.secureRoot, { domain: socket.servername })
      const filepath = resolve(process.cwd(), join(root, selector))
      try {
        await serveFile(socket, filepath)
      } catch (error) {
        if (error.code === 'EISDIR') {
          try {
            await serveFile(socket, join(filepath, 'gophermap'))
          } catch (error) {
            socket.end('3File not found\t\terror.host\t1\r\n.\r\n')
          }
        } else if (error.code === 'ENOENT') {
          socket.end('3File not found\t\terror.host\t1\r\n.\r\n')
        } else {
          socket.end()
        }
      }
    })
  }
}

async function serveFile (socket, filepath) {
  const file = createReadStream(filepath)
  await once(file, 'open')
  file.pipe(socket)
  await once(file, 'close')
  socket.end()
}

module.exports.Gopher = Gopher
