const { app } = require('./app')
const { createSecureServer } = require('http2')
const { SNICallback } = require('./helpers/SNICallback')
const { OCSPRequestHandler } = require('./helpers/ocsp')

module.exports.server = (ticketKeys, options, files) => {
  const requestListener = app(options, files)
  const serverOptions = {
    allowHTTP1: true,
    handshakeTimeout: 12000, // milliseconds, default: 120000
    sessionTimeout: 3600, // seconds, default: 300
    SNICallback: SNICallback(options),
    ticketKeys: Buffer.from(ticketKeys, 'hex')
  }
  const server = createSecureServer(serverOptions, requestListener)
  server.on('OCSPRequest', OCSPRequestHandler)
  if (options.http2.timeout !== null) {
    server.setTimeout(options.http2.timeout)
  }
  process.on('message', ({ type, ticketKeys }) => {
    if (type === 'ticket-keys-refresh') {
      server.setTicketKeys(Buffer.from(ticketKeys, 'hex'))
    }
  })

  return server
}
